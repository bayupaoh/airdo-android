package io.realm;


public interface com_airdo_data_api_model_OrderTangkiRealmProxyInterface {
    public Integer realmGet$id();
    public void realmSet$id(Integer value);
    public String realmGet$driverName();
    public void realmSet$driverName(String value);
    public String realmGet$volume();
    public void realmSet$volume(String value);
    public Integer realmGet$proce();
    public void realmSet$proce(Integer value);
    public boolean realmGet$isChoose();
    public void realmSet$isChoose(boolean value);
}
