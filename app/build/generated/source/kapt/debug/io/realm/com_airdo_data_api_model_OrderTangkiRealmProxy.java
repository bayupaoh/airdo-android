package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_airdo_data_api_model_OrderTangkiRealmProxy extends com.airdo.data.api.model.OrderTangki
    implements RealmObjectProxy, com_airdo_data_api_model_OrderTangkiRealmProxyInterface {

    static final class OrderTangkiColumnInfo extends ColumnInfo {
        long idIndex;
        long driverNameIndex;
        long volumeIndex;
        long proceIndex;
        long isChooseIndex;

        OrderTangkiColumnInfo(OsSchemaInfo schemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("OrderTangki");
            this.idIndex = addColumnDetails("id", "id", objectSchemaInfo);
            this.driverNameIndex = addColumnDetails("driverName", "driverName", objectSchemaInfo);
            this.volumeIndex = addColumnDetails("volume", "volume", objectSchemaInfo);
            this.proceIndex = addColumnDetails("proce", "proce", objectSchemaInfo);
            this.isChooseIndex = addColumnDetails("isChoose", "isChoose", objectSchemaInfo);
        }

        OrderTangkiColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new OrderTangkiColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final OrderTangkiColumnInfo src = (OrderTangkiColumnInfo) rawSrc;
            final OrderTangkiColumnInfo dst = (OrderTangkiColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.driverNameIndex = src.driverNameIndex;
            dst.volumeIndex = src.volumeIndex;
            dst.proceIndex = src.proceIndex;
            dst.isChooseIndex = src.isChooseIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private OrderTangkiColumnInfo columnInfo;
    private ProxyState<com.airdo.data.api.model.OrderTangki> proxyState;

    com_airdo_data_api_model_OrderTangkiRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (OrderTangkiColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.airdo.data.api.model.OrderTangki>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.idIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(Integer value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$driverName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.driverNameIndex);
    }

    @Override
    public void realmSet$driverName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.driverNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.driverNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.driverNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.driverNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$volume() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.volumeIndex);
    }

    @Override
    public void realmSet$volume(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.volumeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.volumeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.volumeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.volumeIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$proce() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.proceIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.proceIndex);
    }

    @Override
    public void realmSet$proce(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.proceIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.proceIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.proceIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.proceIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$isChoose() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.isChooseIndex);
    }

    @Override
    public void realmSet$isChoose(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.isChooseIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.isChooseIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("OrderTangki", 5, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("driverName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("volume", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("proce", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("isChoose", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static OrderTangkiColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new OrderTangkiColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "OrderTangki";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "OrderTangki";
    }

    @SuppressWarnings("cast")
    public static com.airdo.data.api.model.OrderTangki createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.airdo.data.api.model.OrderTangki obj = null;
        if (update) {
            Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
            OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("id")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_airdo_data_api_model_OrderTangkiRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_airdo_data_api_model_OrderTangkiRealmProxy) realm.createObjectInternal(com.airdo.data.api.model.OrderTangki.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_airdo_data_api_model_OrderTangkiRealmProxy) realm.createObjectInternal(com.airdo.data.api.model.OrderTangki.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_airdo_data_api_model_OrderTangkiRealmProxyInterface objProxy = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) obj;
        if (json.has("driverName")) {
            if (json.isNull("driverName")) {
                objProxy.realmSet$driverName(null);
            } else {
                objProxy.realmSet$driverName((String) json.getString("driverName"));
            }
        }
        if (json.has("volume")) {
            if (json.isNull("volume")) {
                objProxy.realmSet$volume(null);
            } else {
                objProxy.realmSet$volume((String) json.getString("volume"));
            }
        }
        if (json.has("proce")) {
            if (json.isNull("proce")) {
                objProxy.realmSet$proce(null);
            } else {
                objProxy.realmSet$proce((int) json.getInt("proce"));
            }
        }
        if (json.has("isChoose")) {
            if (json.isNull("isChoose")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isChoose' to null.");
            } else {
                objProxy.realmSet$isChoose((boolean) json.getBoolean("isChoose"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.airdo.data.api.model.OrderTangki createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.airdo.data.api.model.OrderTangki obj = new com.airdo.data.api.model.OrderTangki();
        final com_airdo_data_api_model_OrderTangkiRealmProxyInterface objProxy = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("driverName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$driverName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$driverName(null);
                }
            } else if (name.equals("volume")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$volume((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$volume(null);
                }
            } else if (name.equals("proce")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$proce((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$proce(null);
                }
            } else if (name.equals("isChoose")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isChoose((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isChoose' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.airdo.data.api.model.OrderTangki copyOrUpdate(Realm realm, com.airdo.data.api.model.OrderTangki object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.airdo.data.api.model.OrderTangki) cachedRealmObject;
        }

        com.airdo.data.api.model.OrderTangki realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
            OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
            long pkColumnIndex = columnInfo.idIndex;
            Number value = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, value.longValue());
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_airdo_data_api_model_OrderTangkiRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.airdo.data.api.model.OrderTangki copy(Realm realm, com.airdo.data.api.model.OrderTangki newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.airdo.data.api.model.OrderTangki) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.airdo.data.api.model.OrderTangki realmObject = realm.createObjectInternal(com.airdo.data.api.model.OrderTangki.class, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) newObject).realmGet$id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        com_airdo_data_api_model_OrderTangkiRealmProxyInterface realmObjectSource = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) newObject;
        com_airdo_data_api_model_OrderTangkiRealmProxyInterface realmObjectCopy = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$driverName(realmObjectSource.realmGet$driverName());
        realmObjectCopy.realmSet$volume(realmObjectSource.realmGet$volume());
        realmObjectCopy.realmSet$proce(realmObjectSource.realmGet$proce());
        realmObjectCopy.realmSet$isChoose(realmObjectSource.realmGet$isChoose());
        return realmObject;
    }

    public static long insert(Realm realm, com.airdo.data.api.model.OrderTangki object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
        long tableNativePtr = table.getNativePtr();
        OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$driverName = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$driverName();
        if (realmGet$driverName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.driverNameIndex, rowIndex, realmGet$driverName, false);
        }
        String realmGet$volume = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$volume();
        if (realmGet$volume != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.volumeIndex, rowIndex, realmGet$volume, false);
        }
        Number realmGet$proce = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$proce();
        if (realmGet$proce != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.proceIndex, rowIndex, realmGet$proce.longValue(), false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isChooseIndex, rowIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$isChoose(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
        long tableNativePtr = table.getNativePtr();
        OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.airdo.data.api.model.OrderTangki object = null;
        while (objects.hasNext()) {
            object = (com.airdo.data.api.model.OrderTangki) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$driverName = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$driverName();
            if (realmGet$driverName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.driverNameIndex, rowIndex, realmGet$driverName, false);
            }
            String realmGet$volume = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$volume();
            if (realmGet$volume != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.volumeIndex, rowIndex, realmGet$volume, false);
            }
            Number realmGet$proce = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$proce();
            if (realmGet$proce != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.proceIndex, rowIndex, realmGet$proce.longValue(), false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isChooseIndex, rowIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$isChoose(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, com.airdo.data.api.model.OrderTangki object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
        long tableNativePtr = table.getNativePtr();
        OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        String realmGet$driverName = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$driverName();
        if (realmGet$driverName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.driverNameIndex, rowIndex, realmGet$driverName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.driverNameIndex, rowIndex, false);
        }
        String realmGet$volume = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$volume();
        if (realmGet$volume != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.volumeIndex, rowIndex, realmGet$volume, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.volumeIndex, rowIndex, false);
        }
        Number realmGet$proce = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$proce();
        if (realmGet$proce != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.proceIndex, rowIndex, realmGet$proce.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.proceIndex, rowIndex, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isChooseIndex, rowIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$isChoose(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.airdo.data.api.model.OrderTangki.class);
        long tableNativePtr = table.getNativePtr();
        OrderTangkiColumnInfo columnInfo = (OrderTangkiColumnInfo) realm.getSchema().getColumnInfo(com.airdo.data.api.model.OrderTangki.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.airdo.data.api.model.OrderTangki object = null;
        while (objects.hasNext()) {
            object = (com.airdo.data.api.model.OrderTangki) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            String realmGet$driverName = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$driverName();
            if (realmGet$driverName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.driverNameIndex, rowIndex, realmGet$driverName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.driverNameIndex, rowIndex, false);
            }
            String realmGet$volume = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$volume();
            if (realmGet$volume != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.volumeIndex, rowIndex, realmGet$volume, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.volumeIndex, rowIndex, false);
            }
            Number realmGet$proce = ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$proce();
            if (realmGet$proce != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.proceIndex, rowIndex, realmGet$proce.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.proceIndex, rowIndex, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isChooseIndex, rowIndex, ((com_airdo_data_api_model_OrderTangkiRealmProxyInterface) object).realmGet$isChoose(), false);
        }
    }

    public static com.airdo.data.api.model.OrderTangki createDetachedCopy(com.airdo.data.api.model.OrderTangki realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.airdo.data.api.model.OrderTangki unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.airdo.data.api.model.OrderTangki();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.airdo.data.api.model.OrderTangki) cachedObject.object;
            }
            unmanagedObject = (com.airdo.data.api.model.OrderTangki) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_airdo_data_api_model_OrderTangkiRealmProxyInterface unmanagedCopy = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) unmanagedObject;
        com_airdo_data_api_model_OrderTangkiRealmProxyInterface realmSource = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$driverName(realmSource.realmGet$driverName());
        unmanagedCopy.realmSet$volume(realmSource.realmGet$volume());
        unmanagedCopy.realmSet$proce(realmSource.realmGet$proce());
        unmanagedCopy.realmSet$isChoose(realmSource.realmGet$isChoose());

        return unmanagedObject;
    }

    static com.airdo.data.api.model.OrderTangki update(Realm realm, com.airdo.data.api.model.OrderTangki realmObject, com.airdo.data.api.model.OrderTangki newObject, Map<RealmModel, RealmObjectProxy> cache) {
        com_airdo_data_api_model_OrderTangkiRealmProxyInterface realmObjectTarget = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) realmObject;
        com_airdo_data_api_model_OrderTangkiRealmProxyInterface realmObjectSource = (com_airdo_data_api_model_OrderTangkiRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$driverName(realmObjectSource.realmGet$driverName());
        realmObjectTarget.realmSet$volume(realmObjectSource.realmGet$volume());
        realmObjectTarget.realmSet$proce(realmObjectSource.realmGet$proce());
        realmObjectTarget.realmSet$isChoose(realmObjectSource.realmGet$isChoose());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("OrderTangki = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{driverName:");
        stringBuilder.append(realmGet$driverName() != null ? realmGet$driverName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{volume:");
        stringBuilder.append(realmGet$volume() != null ? realmGet$volume() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{proce:");
        stringBuilder.append(realmGet$proce() != null ? realmGet$proce() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isChoose:");
        stringBuilder.append(realmGet$isChoose());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_airdo_data_api_model_OrderTangkiRealmProxy aOrderTangki = (com_airdo_data_api_model_OrderTangkiRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aOrderTangki.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aOrderTangki.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aOrderTangki.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
