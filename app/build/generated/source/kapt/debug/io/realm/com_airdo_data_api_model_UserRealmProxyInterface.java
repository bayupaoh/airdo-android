package io.realm;


public interface com_airdo_data_api_model_UserRealmProxyInterface {
    public Integer realmGet$id();
    public void realmSet$id(Integer value);
    public String realmGet$firstName();
    public void realmSet$firstName(String value);
    public String realmGet$lastName();
    public void realmSet$lastName(String value);
    public String realmGet$avatar();
    public void realmSet$avatar(String value);
}
