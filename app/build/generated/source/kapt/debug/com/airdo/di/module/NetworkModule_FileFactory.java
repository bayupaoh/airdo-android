// Generated by Dagger (https://google.github.io/dagger).
package com.airdo.di.module;

import android.content.Context;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import java.io.File;
import javax.inject.Provider;

public final class NetworkModule_FileFactory implements Factory<File> {
  private final NetworkModule module;

  private final Provider<Context> contextProvider;

  public NetworkModule_FileFactory(NetworkModule module, Provider<Context> contextProvider) {
    this.module = module;
    this.contextProvider = contextProvider;
  }

  @Override
  public File get() {
    return provideInstance(module, contextProvider);
  }

  public static File provideInstance(NetworkModule module, Provider<Context> contextProvider) {
    return proxyFile(module, contextProvider.get());
  }

  public static NetworkModule_FileFactory create(
      NetworkModule module, Provider<Context> contextProvider) {
    return new NetworkModule_FileFactory(module, contextProvider);
  }

  public static File proxyFile(NetworkModule instance, Context context) {
    return Preconditions.checkNotNull(
        instance.file(context), "Cannot return null from a non-@Nullable @Provides method");
  }
}
