package com.airdo.feature.fragmentsample;

import java.lang.System;

/**
 * * Created by dodydmw19 on 7/30/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\u0012\u0010\u000f\u001a\u00020\f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/airdo/feature/fragmentsample/SampleActivity;", "Lcom/airdo/base/ui/BaseActivity;", "()V", "mCurrentFragment", "Landroid/support/v4/app/Fragment;", "resourceLayout", "", "getResourceLayout", "()I", "twitterhelper", "", "onViewReady", "", "savedInstanceState", "Landroid/os/Bundle;", "setContentFragment", "fragment", "app_debug"})
public final class SampleActivity extends com.airdo.base.ui.BaseActivity {
    private android.support.v4.app.Fragment mCurrentFragment;
    private java.lang.String twitterhelper;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setContentFragment(android.support.v4.app.Fragment fragment) {
    }
    
    public SampleActivity() {
        super();
    }
}