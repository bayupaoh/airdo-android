package com.airdo.helper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/airdo/helper/CommonConstant;", "", "()V", "APP_CRASH", "", "NOTIFY_FORCE_MESSAGE", "NOTIFY_FORCE_UPDATE", "NOTIFY_NORMAL_MESSAGE", "NOTIFY_NORMAL_UPDATE", "PREFERENCES_IS_TRANSFER", "app_debug"})
public final class CommonConstant {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String APP_CRASH = "app_crash";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NOTIFY_NORMAL_UPDATE = "minimum_info_android";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NOTIFY_FORCE_UPDATE = "minumum_force_android";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NOTIFY_NORMAL_MESSAGE = "info_message";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NOTIFY_FORCE_MESSAGE = "force_message";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PREFERENCES_IS_TRANSFER = "PREFERENCES_IS_TRANSFER";
    public static final com.airdo.helper.CommonConstant INSTANCE = null;
    
    private CommonConstant() {
        super();
    }
}