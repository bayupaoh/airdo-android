package com.airdo.helper;

import java.lang.System;

/**
 * * Created by dodydmw19 on 2/18/19.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/airdo/helper/DefaultExceptionHandler;", "Ljava/lang/Thread$UncaughtExceptionHandler;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "uncaughtException", "", "thread", "Ljava/lang/Thread;", "ex", "", "app_debug"})
public final class DefaultExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
    private final android.app.Activity activity = null;
    
    @java.lang.Override()
    public void uncaughtException(@org.jetbrains.annotations.NotNull()
    java.lang.Thread thread, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable ex) {
    }
    
    public DefaultExceptionHandler(@org.jetbrains.annotations.Nullable()
    android.app.Activity activity) {
        super();
    }
}