package com.airdo.helper;

import java.lang.System;

/**
 * * Created by dodydmw19 on 5/9/19.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ4\u0010\u000b\u001a\u00020\f2\n\u0010\r\u001a\u00060\u000eR\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\nH\u0002J,\u0010\u0014\u001a\u00020\f2\n\u0010\r\u001a\u00060\u000eR\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0005H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/airdo/helper/WrappingRecyclerViewLayoutManager;", "Landroid/support/v7/widget/LinearLayoutManager;", "context", "Landroid/content/Context;", "orientation", "", "reverseLayout", "", "(Landroid/content/Context;IZ)V", "mMeasuredDimension", "", "measureScrapChild", "", "recycler", "Landroid/support/v7/widget/RecyclerView$Recycler;", "Landroid/support/v7/widget/RecyclerView;", "position", "widthSpec", "heightSpec", "measuredDimension", "onMeasure", "state", "Landroid/support/v7/widget/RecyclerView$State;", "app_debug"})
public class WrappingRecyclerViewLayoutManager extends android.support.v7.widget.LinearLayoutManager {
    private final int[] mMeasuredDimension = null;
    
    @java.lang.Override()
    public void onMeasure(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.RecyclerView.Recycler recycler, @org.jetbrains.annotations.NotNull()
    android.support.v7.widget.RecyclerView.State state, int widthSpec, int heightSpec) {
    }
    
    private final void measureScrapChild(android.support.v7.widget.RecyclerView.Recycler recycler, int position, int widthSpec, int heightSpec, int[] measuredDimension) {
    }
    
    public WrappingRecyclerViewLayoutManager(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int orientation, boolean reverseLayout) {
        super(null);
    }
}