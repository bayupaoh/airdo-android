package com.airdo.base.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\b&\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010$\u001a\u00020!2\b\u0010%\u001a\u0004\u0018\u00010#J\b\u0010&\u001a\u00020!H\u0016J\b\u0010\'\u001a\u00020!H\u0016J\u0012\u0010(\u001a\u00020!2\b\u0010)\u001a\u0004\u0018\u00010*H\u0014J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0016J\u0012\u0010/\u001a\u00020!2\b\u0010)\u001a\u0004\u0018\u00010*H$J\u0010\u00100\u001a\u00020!2\u0006\u00101\u001a\u00020\u0016H\u0016J\u0018\u00102\u001a\u00020!2\u0006\u00103\u001a\u00020\u001a2\u0006\u00104\u001a\u00020,H\u0004J\"\u00102\u001a\u00020!2\u0006\u00103\u001a\u00020\u001a2\u0006\u00104\u001a\u00020,2\b\u00105\u001a\u0004\u0018\u000106H\u0003J \u00107\u001a\u00020!2\u0006\u00103\u001a\u00020\u001a2\u0006\u00101\u001a\u0002082\u0006\u00104\u001a\u00020,H\u0004J\u0010\u00109\u001a\u00020!2\u0006\u0010:\u001a\u00020\u001aH\u0005J\u0010\u0010;\u001a\u00020!2\u0006\u0010<\u001a\u00020\u0016H\u0016J\u0010\u0010;\u001a\u00020!2\u0006\u0010<\u001a\u000208H\u0016J\u001e\u0010=\u001a\u00020!2\u0006\u0010<\u001a\u00020\u00162\f\u0010>\u001a\b\u0012\u0004\u0012\u00020!0?H\u0016J\u001e\u0010=\u001a\u00020!2\u0006\u0010<\u001a\u0002082\f\u0010>\u001a\b\u0012\u0004\u0012\u00020!0?H\u0016J\u001e\u0010@\u001a\u00020!2\u0006\u0010<\u001a\u0002082\f\u0010>\u001a\b\u0012\u0004\u0012\u00020!0?H\u0016J)\u0010A\u001a\u00020!2\u0006\u0010B\u001a\u00020,2\b\u0010<\u001a\u0004\u0018\u0001082\b\u0010C\u001a\u0004\u0018\u00010\u0016H\u0016\u00a2\u0006\u0002\u0010DJ\u0010\u0010E\u001a\u00020!2\u0006\u0010F\u001a\u00020\u0016H\u0016J\u0010\u0010E\u001a\u00020!2\u0006\u0010F\u001a\u000208H\u0016J\u0010\u0010G\u001a\u00020!2\u0006\u0010<\u001a\u000208H\u0004R\u0014\u0010\u0004\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8B@BX\u0082\u000e\u00a2\u0006\f\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u00020\u0016X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R(\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a@DX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001f\u00a8\u0006H"}, d2 = {"Lcom/airdo/base/ui/BaseActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lcom/airdo/base/presenter/MvpView;", "()V", "baseFragmentManager", "Landroid/support/v4/app/FragmentManager;", "getBaseFragmentManager", "()Landroid/support/v4/app/FragmentManager;", "mActionBar", "Landroid/support/v7/app/ActionBar;", "mCommonLoadingDialog", "Lcom/airdo/helper/CommonLoadingDialog;", "value", "Landroid/content/Context;", "mContext", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "mInflater", "Landroid/view/LayoutInflater;", "resourceLayout", "", "getResourceLayout", "()I", "<set-?>", "Landroid/support/v7/widget/Toolbar;", "toolBar", "getToolBar", "()Landroid/support/v7/widget/Toolbar;", "setToolBar", "(Landroid/support/v7/widget/Toolbar;)V", "clearRecyclerView", "", "recyclerView", "Lcom/airdo/base/ui/recyclerview/BaseRecyclerView;", "finishLoad", "recycler", "hideLoading", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onViewReady", "setTitle", "title", "setupToolbar", "baseToolbar", "needHomeButton", "onClickListener", "Landroid/view/View$OnClickListener;", "setupToolbarWithTitle", "", "setupToolbarWithoutBackButton", "toolbar", "showAlertDialog", "message", "showConfirmationDialog", "confirmCallback", "Lkotlin/Function0;", "showConfirmationSingleDialog", "showLoading", "isBackPressedCancelable", "currentPage", "(ZLjava/lang/String;Ljava/lang/Integer;)V", "showLoadingWithText", "msg", "showToast", "app_debug"})
public abstract class BaseActivity extends android.support.v7.app.AppCompatActivity implements com.airdo.base.presenter.MvpView {
    @org.jetbrains.annotations.Nullable()
    private android.support.v7.widget.Toolbar toolBar;
    private android.view.LayoutInflater mInflater;
    private android.support.v7.app.ActionBar mActionBar;
    private com.airdo.helper.CommonLoadingDialog mCommonLoadingDialog;
    private java.util.HashMap _$_findViewCache;
    
    private final android.content.Context getMContext() {
        return null;
    }
    
    private final void setMContext(android.content.Context value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.support.v7.widget.Toolbar getToolBar() {
        return null;
    }
    
    protected final void setToolBar(@org.jetbrains.annotations.Nullable()
    android.support.v7.widget.Toolbar p0) {
    }
    
    private final android.support.v4.app.FragmentManager getBaseFragmentManager() {
        return null;
    }
    
    protected abstract int getResourceLayout();
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    protected final void setupToolbar(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.Toolbar baseToolbar, boolean needHomeButton) {
    }
    
    protected final void setupToolbarWithTitle(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.Toolbar baseToolbar, @org.jetbrains.annotations.NotNull()
    java.lang.String title, boolean needHomeButton) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.LOLLIPOP)
    private final void setupToolbar(android.support.v7.widget.Toolbar baseToolbar, boolean needHomeButton, android.view.View.OnClickListener onClickListener) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.LOLLIPOP)
    protected final void setupToolbarWithoutBackButton(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.Toolbar toolbar) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void setTitle(int title) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    protected final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    protected abstract void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState);
    
    @java.lang.Override()
    public void showLoading(boolean isBackPressedCancelable, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Integer currentPage) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    public void showLoadingWithText(int msg) {
    }
    
    @java.lang.Override()
    public void hideLoading() {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationSingleDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showConfirmationDialog(int message, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> confirmCallback) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void showAlertDialog(int message) {
    }
    
    public final void finishLoad(@org.jetbrains.annotations.Nullable()
    com.airdo.base.ui.recyclerview.BaseRecyclerView recycler) {
    }
    
    public final void clearRecyclerView(@org.jetbrains.annotations.Nullable()
    com.airdo.base.ui.recyclerview.BaseRecyclerView recyclerView) {
    }
    
    public BaseActivity() {
        super();
    }
}