package com.airdo.helper.rxbus;

import java.lang.System;

/**
 * * Created by DODYDMW19 on 3/22/2017.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0001J\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\fR\u001c\u0010\u0003\u001a\u0010\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u00010\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/airdo/helper/rxbus/RxBus;", "", "()V", "bus", "Lio/reactivex/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "hasObservers", "", "send", "", "o", "toObservable", "Lio/reactivex/Observable;", "app_debug"})
public final class RxBus {
    private final io.reactivex.subjects.PublishSubject<java.lang.Object> bus = null;
    
    public final void send(@org.jetbrains.annotations.NotNull()
    java.lang.Object o) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<java.lang.Object> toObservable() {
        return null;
    }
    
    public final boolean hasObservers() {
        return false;
    }
    
    public RxBus() {
        super();
    }
}