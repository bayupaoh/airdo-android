package com.airdo.feature.fragmentsample;

import java.lang.System;

/**
 * * Created by dodydmw19 on 7/30/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0005H\u0002J\b\u0010\n\u001a\u00020\u0005H\u0002J\u0012\u0010\u000b\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0014R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u00078TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\t\u00a8\u0006\u000f"}, d2 = {"Lcom/airdo/feature/fragmentsample/SampleFragment;", "Lcom/airdo/base/ui/BaseFragment;", "()V", "actionClicked", "Lkotlin/reflect/KFunction0;", "", "resourceLayout", "", "getResourceLayout", "()I", "dialogPositiveAction", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_debug"})
public final class SampleFragment extends com.airdo.base.ui.BaseFragment {
    private final kotlin.reflect.KFunction<kotlin.Unit> actionClicked = null;
    public static final com.airdo.feature.fragmentsample.SampleFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void dialogPositiveAction() {
    }
    
    private final void actionClicked() {
    }
    
    public SampleFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/airdo/feature/fragmentsample/SampleFragment$Companion;", "", "()V", "newInstance", "Lcom/airdo/base/ui/BaseFragment;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.airdo.base.ui.BaseFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}