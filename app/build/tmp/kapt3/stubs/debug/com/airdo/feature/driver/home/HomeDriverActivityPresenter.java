package com.airdo.feature.driver.home;

import java.lang.System;

/**
 * * Created by SuitTemplate
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u0002H\u0016J\b\u0010\t\u001a\u00020\bH\u0016J\b\u0010\n\u001a\u00020\bH\u0016J\b\u0010\u000b\u001a\u00020\bH\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/airdo/feature/driver/home/HomeDriverActivityPresenter;", "Lcom/airdo/base/presenter/BasePresenter;", "Lcom/airdo/feature/driver/home/HomeDriverActivityView;", "()V", "mCompositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "view", "attachView", "", "detachView", "onDestroy", "onPause", "app_debug"})
public final class HomeDriverActivityPresenter implements com.airdo.base.presenter.BasePresenter<com.airdo.feature.driver.home.HomeDriverActivityView> {
    private com.airdo.feature.driver.home.HomeDriverActivityView view;
    private final io.reactivex.disposables.CompositeDisposable mCompositeDisposable = null;
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.driver.home.HomeDriverActivityView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public HomeDriverActivityPresenter() {
        super();
    }
}