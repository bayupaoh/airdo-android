package com.airdo.feature.user.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 02\u00020\u00012\u00020\u0002:\u00010B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u001a\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u00142\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010\u001e\u001a\u00020\u0018H\u0016J\b\u0010\u001f\u001a\u00020\u0018H\u0016J\b\u0010 \u001a\u00020\u0018H\u0016J\u0012\u0010!\u001a\u00020\u00182\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010$\u001a\u00020\u0018H\u0016J\b\u0010%\u001a\u00020\u0018H\u0016J\b\u0010&\u001a\u00020\u0018H\u0016J\u0012\u0010\'\u001a\u00020\u00182\b\u0010(\u001a\u0004\u0018\u00010)H\u0014J\b\u0010*\u001a\u00020\u0018H\u0002J\b\u0010+\u001a\u00020\u0018H\u0002J\b\u0010,\u001a\u00020\u0018H\u0002J\b\u0010-\u001a\u00020\u0018H\u0002J\b\u0010.\u001a\u00020\u0018H\u0002J\b\u0010/\u001a\u00020\u0018H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00148TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016\u00a8\u00061"}, d2 = {"Lcom/airdo/feature/user/home/HomeFragment;", "Lcom/airdo/base/ui/BaseFragment;", "Lcom/airdo/feature/user/home/OrderTangkiItemView$OnActionListener;", "()V", "adapter", "Lcom/airdo/feature/user/home/OrderTangkiAdapter;", "adapterOrderList", "Lcom/airdo/feature/user/home/OrderListAdapter;", "isOrder", "", "isShow", "isShowDetail", "mBottomSheetBehaviorOrder", "Landroid/support/design/widget/BottomSheetBehavior;", "Landroid/widget/RelativeLayout;", "mBottomSheetBehaviorReview", "mBottomSheetBehaviorStatus", "mapBox", "Lcom/mapbox/mapboxsdk/maps/MapboxMap;", "resourceLayout", "", "getResourceLayout", "()I", "onClicked", "", "view", "Lcom/airdo/feature/user/home/OrderTangkiItemView;", "position", "data", "Lcom/airdo/data/api/model/OrderTangki;", "onDestroy", "onLowMemory", "onPause", "onPrepareOptionsMenu", "menu", "Landroid/view/Menu;", "onResume", "onStart", "onStop", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setupBottomSheet", "setupEmptyView", "setupErrorView", "setupList", "setupMap", "setupOnCLick", "Companion", "app_debug"})
public final class HomeFragment extends com.airdo.base.ui.BaseFragment implements com.airdo.feature.user.home.OrderTangkiItemView.OnActionListener {
    private com.mapbox.mapboxsdk.maps.MapboxMap mapBox;
    private android.support.design.widget.BottomSheetBehavior<android.widget.RelativeLayout> mBottomSheetBehaviorOrder;
    private android.support.design.widget.BottomSheetBehavior<android.widget.RelativeLayout> mBottomSheetBehaviorStatus;
    private android.support.design.widget.BottomSheetBehavior<android.widget.RelativeLayout> mBottomSheetBehaviorReview;
    private com.airdo.feature.user.home.OrderTangkiAdapter adapter;
    private com.airdo.feature.user.home.OrderListAdapter adapterOrderList;
    private boolean isShow;
    private boolean isShowDetail;
    private boolean isOrder;
    public static final com.airdo.feature.user.home.HomeFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onClicked(int position, @org.jetbrains.annotations.Nullable()
    com.airdo.data.api.model.OrderTangki data) {
    }
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupOnCLick() {
    }
    
    private final void setupEmptyView() {
    }
    
    private final void setupErrorView() {
    }
    
    private final void setupList() {
    }
    
    private final void setupBottomSheet() {
    }
    
    private final void setupMap() {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.user.home.OrderTangkiItemView view) {
    }
    
    @java.lang.Override()
    public void onPrepareOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    public void onLowMemory() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public HomeFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/airdo/feature/user/home/HomeFragment$Companion;", "", "()V", "newInstance", "Lcom/airdo/base/ui/BaseFragment;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.airdo.base.ui.BaseFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}