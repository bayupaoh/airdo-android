package com.airdo.feature.driver.review;

import java.lang.System;

/**
 * * Created by SuitTemplate
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u0002H\u0016J\u000e\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0007R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/airdo/feature/driver/review/ReviewActivityItemView;", "Lcom/airdo/base/ui/adapter/viewholder/BaseItemViewHolder;", "", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "mOnActionListener", "Lcom/airdo/feature/driver/review/ReviewActivityItemView$OnActionListener;", "bind", "", "data", "setOnActionListener", "onActionListener", "OnActionListener", "app_debug"})
public final class ReviewActivityItemView extends com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder<java.lang.Object> {
    private com.airdo.feature.driver.review.ReviewActivityItemView.OnActionListener mOnActionListener;
    
    public final void setOnActionListener(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.driver.review.ReviewActivityItemView.OnActionListener onActionListener) {
    }
    
    @java.lang.Override()
    public void bind(@org.jetbrains.annotations.Nullable()
    java.lang.Object data) {
    }
    
    public ReviewActivityItemView(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/airdo/feature/driver/review/ReviewActivityItemView$OnActionListener;", "", "onClicked", "", "view", "Lcom/airdo/feature/driver/review/ReviewActivityItemView;", "app_debug"})
    public static abstract interface OnActionListener {
        
        public abstract void onClicked(@org.jetbrains.annotations.NotNull()
        com.airdo.feature.driver.review.ReviewActivityItemView view);
    }
}