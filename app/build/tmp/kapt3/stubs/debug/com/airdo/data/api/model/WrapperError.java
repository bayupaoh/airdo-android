package com.airdo.data.api.model;

import java.lang.System;

/**
 * * Created by dodydmw19 on 2/11/19.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0015\u0010\t\u001a\u00020\n2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000bJ\u001d\u0010\t\u001a\u00020\n2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\f\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\rJ\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005J\r\u0010\u000f\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u0011\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\u0005J\u0015\u0010\u0012\u001a\u00020\n2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000bR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0013"}, d2 = {"Lcom/airdo/data/api/model/WrapperError;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "()V", "messages", "", "statusCode", "", "Ljava/lang/Long;", "WrapperError", "", "(Ljava/lang/Long;)V", "message", "(Ljava/lang/Long;Ljava/lang/String;)V", "getMessages", "getStatusCode", "()Ljava/lang/Long;", "setMessage", "setStatusCode", "app_debug"})
public final class WrapperError extends java.lang.RuntimeException {
    @com.google.gson.annotations.SerializedName(value = "status_code")
    private java.lang.Long statusCode;
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String messages;
    
    public final void WrapperError(@org.jetbrains.annotations.Nullable()
    java.lang.Long statusCode, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void WrapperError(@org.jetbrains.annotations.Nullable()
    java.lang.Long statusCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getStatusCode() {
        return null;
    }
    
    public final void setStatusCode(@org.jetbrains.annotations.Nullable()
    java.lang.Long statusCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessages() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public WrapperError() {
        super();
    }
}