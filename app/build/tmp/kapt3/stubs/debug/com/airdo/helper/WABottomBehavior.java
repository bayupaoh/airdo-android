package com.airdo.helper;

import java.lang.System;

/**
 * * Created by King Oil on 15/12/2017.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\t\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ%\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\u000f\u001a\u00020\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011J5\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0016\u00a2\u0006\u0002\u0010\u0018J=\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016\u00a2\u0006\u0002\u0010 J5\u0010!\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010#\u001a\u00020\u001cH\u0016\u00a2\u0006\u0002\u0010$J%\u0010%\u001a\u00020\u001a2\u0006\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\u0014\u001a\u00020\u0002H\u0016\u00a2\u0006\u0002\u0010&J%\u0010\'\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0006\u0010\u000f\u001a\u00020\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011\u00a8\u0006("}, d2 = {"Lcom/airdo/helper/WABottomBehavior;", "V", "Landroid/view/View;", "Landroid/support/design/widget/BottomSheetBehavior;", "()V", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "onInterceptTouchEvent", "", "parent", "Landroid/support/design/widget/CoordinatorLayout;", "child", "event", "Landroid/view/MotionEvent;", "(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z", "onNestedPreFling", "coordinatorLayout", "target", "velocityX", "", "velocityY", "(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;FF)Z", "onNestedPreScroll", "", "dx", "", "dy", "consumed", "", "(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[I)V", "onStartNestedScroll", "directTargetChild", "nestedScrollAxes", "(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z", "onStopNestedScroll", "(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V", "onTouchEvent", "app_debug"})
public final class WABottomBehavior<V extends android.view.View> extends android.support.design.widget.BottomSheetBehavior<V> {
    
    @java.lang.Override()
    public boolean onInterceptTouchEvent(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout parent, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onTouchEvent(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout parent, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onStartNestedScroll(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout coordinatorLayout, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.View directTargetChild, @org.jetbrains.annotations.NotNull()
    android.view.View target, int nestedScrollAxes) {
        return false;
    }
    
    @java.lang.Override()
    public void onNestedPreScroll(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout coordinatorLayout, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.View target, int dx, int dy, @org.jetbrains.annotations.NotNull()
    int[] consumed) {
    }
    
    @java.lang.Override()
    public void onStopNestedScroll(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout coordinatorLayout, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.View target) {
    }
    
    @java.lang.Override()
    public boolean onNestedPreFling(@org.jetbrains.annotations.NotNull()
    android.support.design.widget.CoordinatorLayout coordinatorLayout, @org.jetbrains.annotations.NotNull()
    V child, @org.jetbrains.annotations.NotNull()
    android.view.View target, float velocityX, float velocityY) {
        return false;
    }
    
    public WABottomBehavior() {
        super();
    }
    
    public WABottomBehavior(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super();
    }
}