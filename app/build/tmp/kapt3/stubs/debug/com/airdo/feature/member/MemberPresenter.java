package com.airdo.feature.member;

import java.lang.System;

/**
 * * Created by DODYDMW19 on 1/30/2018.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0016J\b\u0010\u0013\u001a\u00020\u0011H\u0016J\u0015\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u0010\u0017J\u0006\u0010\u0018\u001a\u00020\u0011J\b\u0010\u0019\u001a\u00020\u0011H\u0016J\b\u0010\u001a\u001a\u00020\u0011H\u0016J\'\u0010\u001b\u001a\u00020\u00112\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u001d2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002\u00a2\u0006\u0002\u0010\u001eR\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/airdo/feature/member/MemberPresenter;", "Lcom/airdo/base/presenter/BasePresenter;", "Lcom/airdo/feature/member/MemberView;", "()V", "apiService", "Lcom/airdo/data/api/APIService;", "getApiService", "()Lcom/airdo/data/api/APIService;", "setApiService", "(Lcom/airdo/data/api/APIService;)V", "mCompositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "mRealm", "Lcom/airdo/data/localdb/RealmHelper;", "Lcom/airdo/data/api/model/User;", "mvpView", "attachView", "", "view", "detachView", "getMember", "currentPage", "", "(Ljava/lang/Integer;)V", "getMemberCache", "onDestroy", "onPause", "saveToCache", "data", "", "(Ljava/util/List;Ljava/lang/Integer;)V", "app_debug"})
public final class MemberPresenter implements com.airdo.base.presenter.BasePresenter<com.airdo.feature.member.MemberView> {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.airdo.data.api.APIService apiService;
    private com.airdo.feature.member.MemberView mvpView;
    private com.airdo.data.localdb.RealmHelper<com.airdo.data.api.model.User> mRealm;
    private io.reactivex.disposables.CompositeDisposable mCompositeDisposable;
    
    @org.jetbrains.annotations.NotNull()
    public final com.airdo.data.api.APIService getApiService() {
        return null;
    }
    
    public final void setApiService(@org.jetbrains.annotations.NotNull()
    com.airdo.data.api.APIService p0) {
    }
    
    public final void getMemberCache() {
    }
    
    public final void getMember(@org.jetbrains.annotations.Nullable()
    java.lang.Integer currentPage) {
    }
    
    private final void saveToCache(java.util.List<? extends com.airdo.data.api.model.User> data, java.lang.Integer currentPage) {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.member.MemberView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public MemberPresenter() {
        super();
    }
}