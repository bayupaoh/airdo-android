package com.airdo.helper;

import java.lang.System;

/**
 * * Created by dodydmw19 on 7/18/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/airdo/helper/CommonUtils;", "", "()V", "Companion", "app_debug"})
public final class CommonUtils {
    public static final com.airdo.helper.CommonUtils.Companion Companion = null;
    
    public CommonUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nJ \u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00102\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019J(\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u000e2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019J8\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u00a8\u0006\u001d"}, d2 = {"Lcom/airdo/helper/CommonUtils$Companion;", "", "()V", "changeProgressBarColor", "", "color", "", "progressBar", "Landroid/widget/ProgressBar;", "context", "Landroid/content/Context;", "checkTwitterApp", "", "getCompleteAddressString", "", "LATITUDE", "", "LONGITUDE", "getKey", "", "openAppInStore", "setCamera", "lat", "lng", "mapBox", "Lcom/mapbox/mapboxsdk/maps/MapboxMap;", "setMarker", "title", "iconDrawable", "app_debug"})
    public static final class Companion {
        
        public final boolean checkTwitterApp(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return false;
        }
        
        public final void openAppInStore(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final byte[] getKey() {
            return null;
        }
        
        public final void changeProgressBarColor(int color, @org.jetbrains.annotations.NotNull()
        android.widget.ProgressBar progressBar, @org.jetbrains.annotations.Nullable()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getCompleteAddressString(double LATITUDE, double LONGITUDE, @org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        public final void setMarker(double lat, double lng, @org.jetbrains.annotations.NotNull()
        java.lang.String title, @org.jetbrains.annotations.Nullable()
        com.mapbox.mapboxsdk.maps.MapboxMap mapBox) {
        }
        
        public final void setMarker(double lat, double lng, @org.jetbrains.annotations.NotNull()
        java.lang.String title, int iconDrawable, @org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        com.mapbox.mapboxsdk.maps.MapboxMap mapBox) {
        }
        
        public final void setCamera(double lat, double lng, @org.jetbrains.annotations.Nullable()
        com.mapbox.mapboxsdk.maps.MapboxMap mapBox) {
        }
        
        private Companion() {
            super();
        }
    }
}