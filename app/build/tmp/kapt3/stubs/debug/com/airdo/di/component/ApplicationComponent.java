package com.airdo.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&\u00a8\u0006\n"}, d2 = {"Lcom/airdo/di/component/ApplicationComponent;", "", "inject", "", "loginPresenter", "Lcom/airdo/feature/login/LoginPresenter;", "memberPresenter", "Lcom/airdo/feature/member/MemberPresenter;", "splashScreenPresenter", "Lcom/airdo/feature/splashscreen/SplashScreenPresenter;", "app_debug"})
@dagger.Component(modules = {com.airdo.di.module.APIServiceModule.class})
@com.airdo.di.scope.SuitCoreApplicationScope()
public abstract interface ApplicationComponent {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.member.MemberPresenter memberPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.login.LoginPresenter loginPresenter);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.splashscreen.SplashScreenPresenter splashScreenPresenter);
}