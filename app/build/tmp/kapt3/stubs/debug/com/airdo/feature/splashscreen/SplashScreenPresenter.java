package com.airdo.feature.splashscreen;

import java.lang.System;

/**
 * * Created by dodydmw19 on 12/19/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016J\b\u0010\f\u001a\u00020\nH\u0016J\u0006\u0010\r\u001a\u00020\nJ\b\u0010\u000e\u001a\u00020\nH\u0016J\b\u0010\u000f\u001a\u00020\nH\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/airdo/feature/splashscreen/SplashScreenPresenter;", "Lcom/airdo/base/presenter/BasePresenter;", "Lcom/airdo/feature/splashscreen/SplashScreenView;", "()V", "SPLASH_TIME", "", "getSPLASH_TIME", "()J", "mvpView", "attachView", "", "view", "detachView", "initialize", "onDestroy", "onPause", "app_debug"})
public final class SplashScreenPresenter implements com.airdo.base.presenter.BasePresenter<com.airdo.feature.splashscreen.SplashScreenView> {
    private com.airdo.feature.splashscreen.SplashScreenView mvpView;
    private final long SPLASH_TIME = 3000L;
    
    public final long getSPLASH_TIME() {
        return 0L;
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public final void initialize() {
    }
    
    @java.lang.Override()
    public void attachView(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.splashscreen.SplashScreenView view) {
    }
    
    @java.lang.Override()
    public void detachView() {
    }
    
    public SplashScreenPresenter() {
        super();
    }
}