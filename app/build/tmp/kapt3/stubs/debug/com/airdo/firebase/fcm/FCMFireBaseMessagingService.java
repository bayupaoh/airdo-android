package com.airdo.firebase.fcm;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J \u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0003\u00a8\u0006\r"}, d2 = {"Lcom/airdo/firebase/fcm/FCMFireBaseMessagingService;", "Lcom/google/firebase/messaging/FirebaseMessagingService;", "()V", "onMessageReceived", "", "remoteMessage", "Lcom/google/firebase/messaging/RemoteMessage;", "sendNotification", "messageBody", "", "id", "title", "Companion", "app_debug"})
public final class FCMFireBaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final java.lang.String TAG = "MyFirebaseMsgService";
    public static final com.airdo.firebase.fcm.FCMFireBaseMessagingService.Companion Companion = null;
    
    /**
     * * Called when message is received.
     *     *
     *     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @java.lang.Override()
    public void onMessageReceived(@org.jetbrains.annotations.Nullable()
    com.google.firebase.messaging.RemoteMessage remoteMessage) {
    }
    
    /**
     * * Create and show a simple notification containing the received FCM message.
     *     *
     *     * @param messageBody FCM message body received.
     */
    @android.annotation.SuppressLint(value = {"CheckResult"})
    private final void sendNotification(java.lang.String messageBody, java.lang.String id, java.lang.String title) {
    }
    
    public FCMFireBaseMessagingService() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/airdo/firebase/fcm/FCMFireBaseMessagingService$Companion;", "", "()V", "TAG", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}