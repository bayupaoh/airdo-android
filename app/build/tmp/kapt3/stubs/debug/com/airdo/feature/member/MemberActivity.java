package com.airdo.feature.member;

import java.lang.System;

/**
 * * Created by DODYDMW19 on 1/30/2018.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 (2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001(B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0006H\u0002J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u000fH\u0014J\u0012\u0010\u0015\u001a\u00020\u000f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u000f2\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0016J\b\u0010\u001c\u001a\u00020\u000fH\u0016J\u0018\u0010\u001d\u001a\u00020\u000f2\u000e\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0016J\u0012\u0010\u001e\u001a\u00020\u000f2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0018\u0010!\u001a\u00020\u000f2\u000e\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001aH\u0002J\b\u0010#\u001a\u00020\u000fH\u0002J\b\u0010$\u001a\u00020\u000fH\u0002J\b\u0010%\u001a\u00020\u000fH\u0002J\b\u0010&\u001a\u00020\u000fH\u0002J\b\u0010\'\u001a\u00020\u000fH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u0006X\u0094D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r\u00a8\u0006)"}, d2 = {"Lcom/airdo/feature/member/MemberActivity;", "Lcom/airdo/base/ui/BaseActivity;", "Lcom/airdo/feature/member/MemberView;", "Lcom/airdo/feature/member/MemberItemView$OnActionListener;", "()V", "currentPage", "", "memberAdapter", "Lcom/airdo/feature/member/MemberAdapter;", "memberPresenter", "Lcom/airdo/feature/member/MemberPresenter;", "resourceLayout", "getResourceLayout", "()I", "loadData", "", "page", "onClicked", "view", "Lcom/airdo/feature/member/MemberItemView;", "onDestroy", "onFailed", "error", "", "onMemberCacheLoaded", "members", "", "Lcom/airdo/data/api/model/User;", "onMemberEmpty", "onMemberLoaded", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setData", "data", "setupEmptyView", "setupErrorView", "setupList", "setupPresenter", "setupProgressView", "Companion", "app_debug"})
public final class MemberActivity extends com.airdo.base.ui.BaseActivity implements com.airdo.feature.member.MemberView, com.airdo.feature.member.MemberItemView.OnActionListener {
    private com.airdo.feature.member.MemberPresenter memberPresenter;
    private int currentPage;
    private com.airdo.feature.member.MemberAdapter memberAdapter;
    private final int resourceLayout = 2131492902;
    public static final com.airdo.feature.member.MemberActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void setupPresenter() {
    }
    
    private final void setupList() {
    }
    
    private final void loadData(int page) {
    }
    
    private final void setData(java.util.List<? extends com.airdo.data.api.model.User> data) {
    }
    
    private final void setupProgressView() {
    }
    
    private final void setupEmptyView() {
    }
    
    private final void setupErrorView() {
    }
    
    @java.lang.Override()
    public void onMemberCacheLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.airdo.data.api.model.User> members) {
    }
    
    @java.lang.Override()
    public void onMemberLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.airdo.data.api.model.User> members) {
    }
    
    @java.lang.Override()
    public void onMemberEmpty() {
    }
    
    @java.lang.Override()
    public void onFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error) {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.Nullable()
    com.airdo.feature.member.MemberItemView view) {
    }
    
    public MemberActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/airdo/feature/member/MemberActivity$Companion;", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent createIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}