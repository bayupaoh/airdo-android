package com.airdo.feature.member;

import java.lang.System;

/**
 * * Created by DODYDMW19 on 1/30/2018.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH&J\b\u0010\n\u001a\u00020\u0003H&J\u0018\u0010\u000b\u001a\u00020\u00032\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH&\u00a8\u0006\f"}, d2 = {"Lcom/airdo/feature/member/MemberView;", "Lcom/airdo/base/presenter/MvpView;", "onFailed", "", "error", "", "onMemberCacheLoaded", "members", "", "Lcom/airdo/data/api/model/User;", "onMemberEmpty", "onMemberLoaded", "app_debug"})
public abstract interface MemberView extends com.airdo.base.presenter.MvpView {
    
    public abstract void onMemberCacheLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.airdo.data.api.model.User> members);
    
    public abstract void onMemberLoaded(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.airdo.data.api.model.User> members);
    
    public abstract void onMemberEmpty();
    
    public abstract void onFailed(@org.jetbrains.annotations.Nullable()
    java.lang.Object error);
}