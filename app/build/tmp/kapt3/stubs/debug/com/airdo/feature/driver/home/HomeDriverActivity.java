package com.airdo.feature.driver.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 $2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001$B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\u0018\u001a\u00020\u0015H\u0014J\u0012\u0010\u0019\u001a\u00020\u00152\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0014J\b\u0010\u001c\u001a\u00020\u0015H\u0002J\b\u0010\u001d\u001a\u00020\u0015H\u0002J\b\u0010\u001e\u001a\u00020\u0015H\u0002J\b\u0010\u001f\u001a\u00020\u0015H\u0002J\b\u0010 \u001a\u00020\u0015H\u0002J\b\u0010!\u001a\u00020\u0015H\u0002J\b\u0010\"\u001a\u00020\u0015H\u0002J\b\u0010#\u001a\u00020\u0015H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006%"}, d2 = {"Lcom/airdo/feature/driver/home/HomeDriverActivity;", "Lcom/airdo/base/ui/BaseActivity;", "Lcom/airdo/feature/driver/home/HomeDriverActivityView;", "Lcom/airdo/feature/user/history/HistoryItemView$OnActionListener;", "()V", "adapterHistory", "Lcom/airdo/feature/driver/history/HistoryDriverActivityAdapter;", "adapterReview", "Lcom/airdo/feature/driver/review/ReviewActivityAdapter;", "mBottomSheetBehaviorOrder", "Landroid/support/design/widget/BottomSheetBehavior;", "Landroid/widget/RelativeLayout;", "presenter", "Lcom/airdo/feature/driver/home/HomeDriverActivityPresenter;", "resourceLayout", "", "getResourceLayout", "()I", "getContext", "Landroid/content/Context;", "onClicked", "", "view", "Lcom/airdo/feature/user/history/HistoryItemView;", "onDestroy", "onViewReady", "savedInstanceState", "Landroid/os/Bundle;", "setupBottomSheet", "setupEmptyView", "setupErrorView", "setupList", "setupOnCLick", "setupPresenter", "setupProgressView", "setupToolbar", "Companion", "app_debug"})
public final class HomeDriverActivity extends com.airdo.base.ui.BaseActivity implements com.airdo.feature.driver.home.HomeDriverActivityView, com.airdo.feature.user.history.HistoryItemView.OnActionListener {
    private com.airdo.feature.driver.home.HomeDriverActivityPresenter presenter;
    private android.support.design.widget.BottomSheetBehavior<android.widget.RelativeLayout> mBottomSheetBehaviorOrder;
    private com.airdo.feature.driver.history.HistoryDriverActivityAdapter adapterHistory;
    private com.airdo.feature.driver.review.ReviewActivityAdapter adapterReview;
    public static final com.airdo.feature.driver.home.HomeDriverActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getResourceLayout() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onViewReady(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupToolbar() {
    }
    
    private final void setupOnCLick() {
    }
    
    private final void setupBottomSheet() {
    }
    
    private final void setupList() {
    }
    
    private final void setupProgressView() {
    }
    
    private final void setupEmptyView() {
    }
    
    private final void setupErrorView() {
    }
    
    private final void setupPresenter() {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.NotNull()
    com.airdo.feature.user.history.HistoryItemView view) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public HomeDriverActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/airdo/feature/driver/home/HomeDriverActivity$Companion;", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent createIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}