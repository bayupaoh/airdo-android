package com.airdo.feature.login;

import java.lang.System;

/**
 * * Created by dodydmw19 on 7/18/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/airdo/feature/login/LoginView;", "Lcom/airdo/base/presenter/MvpView;", "app_debug"})
public abstract interface LoginView extends com.airdo.base.presenter.MvpView {
}