package com.airdo.data.prefs;

import java.lang.System;

/**
 * * Created by dodydmw19 on 7/23/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/airdo/data/prefs/DataConstant;", "", "()V", "KEY_MEMBER_CACHE", "", "RANDOM_KEY", "app_debug"})
public final class DataConstant {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_MEMBER_CACHE = "member_list";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RANDOM_KEY = "random_key";
    public static final com.airdo.data.prefs.DataConstant INSTANCE = null;
    
    private DataConstant() {
        super();
    }
}