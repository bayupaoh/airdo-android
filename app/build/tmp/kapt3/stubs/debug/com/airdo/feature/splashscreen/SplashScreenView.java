package com.airdo.feature.splashscreen;

import java.lang.System;

/**
 * * Created by dodydmw19 on 12/19/18.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/airdo/feature/splashscreen/SplashScreenView;", "Lcom/airdo/base/presenter/MvpView;", "navigateToMainView", "", "app_debug"})
public abstract interface SplashScreenView extends com.airdo.base.presenter.MvpView {
    
    public abstract void navigateToMainView();
}