package com.airdo.data.api.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class OrderTangki : RealmObject(){
    @PrimaryKey
    var id: Int? = 0
    var driverName: String? = ""
    var volume: String? = ""
    var proce: Int? = 0
    var isChoose: Boolean = false
}