package com.airdo.data.prefs

class SuitException(message: String?) : RuntimeException(message)