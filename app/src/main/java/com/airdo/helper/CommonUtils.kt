package com.airdo.helper

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.NameNotFoundException
import android.graphics.PorterDuff
import android.location.Geocoder
import android.net.Uri
import android.widget.ProgressBar
import com.airdo.data.prefs.DataConstant
import com.airdo.data.prefs.SuitPreferences
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import java.security.SecureRandom
import java.util.*
import android.app.WallpaperColors.fromDrawable
import android.R
import android.support.v4.content.ContextCompat
import android.graphics.drawable.Drawable
import com.mapbox.mapboxsdk.annotations.IconFactory



/**
 * Created by dodydmw19 on 7/18/18.
 */

class CommonUtils {

    companion object {

        fun checkTwitterApp(context: Context): Boolean {
            return try {
                var info = context.packageManager.getApplicationInfo("com.twitter.android", 0)
                true
            } catch (e: NameNotFoundException) {
                false
            }
        }

        fun openAppInStore(context: Context) {
            // you can also use BuildConfig.APPLICATION_ID
            try {
                val appId = context.packageName
                val rateIntent = Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appId"))
                var marketFound = false
                // find all applications able to handle our rateIntent
                val otherApps = context.packageManager
                        .queryIntentActivities(rateIntent, 0)
                for (otherApp in otherApps) {
                    // look for Google Play application
                    if (otherApp.activityInfo.applicationInfo.packageName == "com.android.vending") {

                        val otherAppActivity = otherApp.activityInfo
                        val componentName = ComponentName(
                                otherAppActivity.applicationInfo.packageName,
                                otherAppActivity.name
                        )
                        // make sure it does NOT open in the stack of your activity
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        // task reparenting if needed
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                        // if the Google Play was already open in a search result
                        //  this make sure it still go to the app page you requested
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        // this make sure only the Google Play app is allowed to
                        // intercept the intent
                        rateIntent.component = componentName
                        context.startActivity(rateIntent)
                        marketFound = true
                        break

                    }
                }

                // if GP not present on device, open web browser
                if (!marketFound) {
                    val webIntent = Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appId"))
                    context.startActivity(webIntent)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun getKey() : ByteArray{
            // Generate a random encryption key
            val key = ByteArray(64)
            SecureRandom().nextBytes(key)

            return if(SuitPreferences.instance()?.getString(DataConstant.RANDOM_KEY) != null &&
                    SuitPreferences.instance()?.getString(DataConstant.RANDOM_KEY).toString().isNotEmpty()){
                SuitPreferences.instance()?.getObject(DataConstant.RANDOM_KEY, ByteArray::class.java)!!
            }else{
                SuitPreferences.instance()?.saveObject(DataConstant.RANDOM_KEY, key)
                key
            }

        }

        fun changeProgressBarColor(color : Int, progressBar : ProgressBar, context: Context?){
            context?.let {ctx ->
                progressBar.indeterminateDrawable.
                        setColorFilter(ctx.resources.getColor(color), PorterDuff.Mode.SRC_IN)
            }

        }

        fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double, context: Context): String {
            var strAdd = ""
            val geocoder = Geocoder(context, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
                if (addresses != null) {
                    val returnedAddress = addresses[0]
                    val strReturnedAddress = StringBuilder("")

                    for (i in 0..returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                    strAdd = strReturnedAddress.toString()
                } else {
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return strAdd
        }

        fun setMarker(lat: Double, lng: Double, title: String, mapBox: MapboxMap?) {
            mapBox?.addMarker(MarkerOptions()
                    .position(LatLng(lat, lng))
                    .title(title)
                    .snippet(title))
        }

        fun setMarker(lat: Double, lng: Double, title: String, iconDrawable: Int, context: Context, mapBox: MapboxMap?) {
            val iconFactory = IconFactory.getInstance(context)
            val icon = iconFactory.fromResource(iconDrawable)

            mapBox?.addMarker(MarkerOptions()
                    .position(LatLng(lat, lng))
                    .title(title)
                    .snippet(title)
                    .icon(icon))
        }

        fun setCamera(lat: Double, lng: Double, mapBox: MapboxMap?) {
            val position = CameraPosition.Builder()
                    .target(LatLng(lat, lng))
                    .zoom(13.0)
                    .build()

            mapBox?.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position), 3000)
        }
    }

}