package com.airdo.helper

object CommonConstant {

    // for intent extras constant
    const val APP_CRASH = "app_crash"

    // -- Remote Config Params
    const val NOTIFY_NORMAL_UPDATE = "minimum_info_android"
    const val NOTIFY_FORCE_UPDATE = "minumum_force_android"
    const val NOTIFY_NORMAL_MESSAGE = "info_message"
    const val NOTIFY_FORCE_MESSAGE = "force_message"

    //-- Prefrences
    const val PREFERENCES_IS_TRANSFER = "PREFERENCES_IS_TRANSFER"
}