package com.airdo

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.facebook.drawee.backends.pipeline.Fresco
import com.airdo.data.prefs.SuitPreferences
import com.airdo.di.component.ApplicationComponent
import com.airdo.di.component.DaggerApplicationComponent
import com.airdo.di.module.ContextModule
import com.mapbox.mapboxsdk.Mapbox
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by DODYDMW19 on 1/30/2018.
 */

class BaseApplication : MultiDexApplication() {

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder().contextModule(ContextModule(this)).build()

        Fresco.initialize(this)

        // Initial Preferences
        SuitPreferences.init(applicationContext)

        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                //.encryptionKey(CommonUtils.getKey()) // encrypt realm
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
        Mapbox.getInstance(this, "pk.eyJ1IjoiYmF5dXBhb2giLCJhIjoiY2p4YjZtcDh0MDNjODNybzduYXk5MDk2eCJ9.ZLNNJD7jvHr86VoTfSxZUQ")
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}