package com.airdo.di.component

import com.airdo.di.module.APIServiceModule
import com.airdo.di.scope.SuitCoreApplicationScope
import com.airdo.feature.login.LoginPresenter
import com.airdo.feature.member.MemberPresenter
import com.airdo.feature.splashscreen.SplashScreenPresenter
import dagger.Component

@SuitCoreApplicationScope
@Component(modules = [(APIServiceModule::class)])
interface ApplicationComponent {

    fun inject(memberPresenter: MemberPresenter)

    fun inject(loginPresenter: LoginPresenter)

    fun inject(splashScreenPresenter: SplashScreenPresenter)
}