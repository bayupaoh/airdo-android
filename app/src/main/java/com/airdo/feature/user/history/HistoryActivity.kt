package com.airdo.feature.user.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import com.airdo.feature.user.detailhistory.DetailHistoryActivity
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class HistoryActivity : BaseActivity(), HistoryActivityView, HistoryItemView.OnActionListener {

    private var presenter: HistoryActivityPresenter? = null
    private var adapter : HistoryAdapter ?= HistoryAdapter()

    override val resourceLayout: Int
        get() = R.layout.activity_history;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(historyToolbar, true)
        setupEmptyView()
        setupErrorView()
        setupList()
        setupPresenter()
    }

    private fun setupList() {
        var list = mutableListOf<Any>()
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")

        recHistory.apply {
            setUpAsList()
            setAdapter(adapter)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }
        adapter?.add(list)
        adapter?.setOnActionListener(this)

        recHistory?.let {
            it.completeRefresh()
            it.loadMoreComplete()
            it.stopShimmer()
        }

        recHistory.showRecycler()

    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        recHistory.setImageEmptyView(R.drawable.empty_state)
        recHistory.setTextEmptyView(getString(R.string.txt_empty_member))
        recHistory.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupErrorView() {
        recHistory.setImageErrorView(R.drawable.empty_state)
        recHistory.setTExtErrorView(getString(R.string.txt_error_connection))
        recHistory.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupPresenter() {
        presenter = HistoryActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onClicked(view: HistoryItemView) {
        startActivity(DetailHistoryActivity.createIntent(this))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, HistoryActivity::class.java)
        }
    }
}