package com.airdo.feature.user.home;

import android.view.ViewGroup
import com.airdo.R;
import com.airdo.base.ui.adapter.BaseRecyclerAdapter;
import com.airdo.data.api.model.OrderTangki

/**
 * Created by SuitTemplate
 */

public class OrderListAdapter : BaseRecyclerAdapter<OrderTangki, OrderListItemView>() {
    private var mOnActionListener: OrderListItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: OrderListItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_order_list


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListItemView {
        val view = OrderListItemView(getView(parent!!))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}