package com.airdo.feature.user.detailhistory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import kotlinx.android.synthetic.main.activity_detail_history.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class DetailHistoryActivity : BaseActivity(), DetailHistoryActivityView {

    private var presenter: DetailHistoryActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_detail_history;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(historyDetailToolbar, true)
        setupPresenter()
    }

    private fun setupPresenter() {
        presenter = DetailHistoryActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, DetailHistoryActivity::class.java)
        }
    }
}