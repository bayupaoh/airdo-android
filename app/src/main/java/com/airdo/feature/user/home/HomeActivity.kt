package com.airdo.feature.user.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import com.airdo.data.api.model.OrderTangki
import com.airdo.feature.user.history.HistoryActivity
import com.airdo.feature.user.setting.SettingActivity
import com.airdo.helper.CommonUtils
import com.google.gson.Gson
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity(), OrderTangkiItemView.OnActionListener {
    override fun onClicked(position: Int, data: OrderTangki?) {

    }

    private var mapBox: MapboxMap? = null
    private var mBottomSheetBehaviorOrder: BottomSheetBehavior<RelativeLayout>? = null
    private var mBottomSheetBehaviorStatus: BottomSheetBehavior<RelativeLayout>? = null
    private var mBottomSheetBehaviorReview: BottomSheetBehavior<RelativeLayout>? = null
    private var adapter: OrderTangkiAdapter? = OrderTangkiAdapter()
    private var adapterOrderList: OrderListAdapter? = OrderListAdapter()
    private var isShow: Boolean = false
    private var isShowDetail: Boolean = false
    private var isOrder: Boolean = true

    override val resourceLayout: Int
        get() = R.layout.activity_home;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(mainToolbar, false)
        showAlertDialog("Klik marker yang bergambar mobil untuk memesan air")
//        setContentFragment(HomeFragment.newInstance())
        setupBottomSheet()
        setupEmptyView()
        setupErrorView()
        setupList()
        setupMap()
        setupOnCLick()

    }

//    private fun setContentFragment(fragment: Fragment?) {
//        mCurrentFragment = fragment
//        if (mCurrentFragment != null) {
//            supportFragmentManager
//                    .beginTransaction()
//                    .replace(R.id.mainFrame, mCurrentFragment!!)
//                    .commitAllowingStateLoss()
//        }
//    }

    private fun setupOnCLick() {
        homeOrderSubmit.setOnClickListener {
            isOrder = false
            mapBox?.clear()
            CommonUtils.setMarker(-10.1556097, 123.6170343, "Air tangki Pieter", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6061854, "Your Position", R.drawable.ic_location_blue, this, mapBox)
            CommonUtils.setCamera(-10.1556097, 123.6061854, mapBox)
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
            mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_HALF_EXPANDED
            val list = mutableListOf<OrderTangki>()
            list.add(OrderTangki())
            list.add(OrderTangki())
            list.add(OrderTangki())
            adapterOrderList?.clear()
            adapterOrderList?.addAll(list)
            rvStatusOrder?.let {
                it.completeRefresh()
                it.loadMoreComplete()
                it.stopShimmer()
            }

            rvStatusOrder.showRecycler()
        }

        homeStatusFormClose.setOnClickListener {
            isShow = !isShow
            if (isShow) {
                homeStatusFormClose.setImageResource(R.drawable.ic_arrow_down)
                mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                homeStatusFormClose.setImageResource(R.drawable.ic_arrow_up)
                homeStatusDetail.visibility = View.GONE
                mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_HALF_EXPANDED
            }
        }

        homeReviewClose.setOnClickListener {
            mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_HALF_EXPANDED
            mBottomSheetBehaviorReview?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        homeStatusShow.setOnClickListener {
            isShowDetail = !isShowDetail
            if (isShowDetail) {
                mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_EXPANDED
                homeStatusDetail.visibility = View.VISIBLE
            } else {
                homeStatusDetail.visibility = View.GONE
            }
        }

        homeFinishOrderSubmit.setOnClickListener {
            mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_HIDDEN
            mBottomSheetBehaviorReview?.state = BottomSheetBehavior.STATE_EXPANDED
        }

        reviewSubmit.setOnClickListener {
            showAlertDialog("Terima kasih telah melakukan orderan. Silahkan klik marker untuk membeli air lagi")
            isOrder = true
            mapBox?.clear()
            setupBottomSheet()
            CommonUtils.setMarker(-10.1612841, 123.6061745, "Air tangki Nostalgia", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1603677, 123.6081796, "Air tangki Gloria", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1545117, 123.6176783, "Air tangki Om Nadus", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6170343, "Air tangki Pieter", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6061854, "Your Position", R.drawable.ic_location_blue, this, mapBox)
        }
    }

    private fun setupEmptyView() {
        rvOrder.setImageEmptyView(R.drawable.empty_state)
        rvOrder.setTextEmptyView(getString(R.string.txt_empty_member))
        rvOrder.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
            }

        })

        rvStatusOrder.setImageEmptyView(R.drawable.empty_state)
        rvStatusOrder.setTextEmptyView(getString(R.string.txt_empty_member))
        rvStatusOrder.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
            }

        })

    }

    private fun setupErrorView() {
        rvOrder.setImageErrorView(R.drawable.empty_state)
        rvOrder.setTExtErrorView(getString(R.string.txt_error_connection))
        rvOrder.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
            }

        })

        rvStatusOrder.setImageErrorView(R.drawable.empty_state)
        rvStatusOrder.setTExtErrorView(getString(R.string.txt_error_connection))
        rvStatusOrder.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
            }

        })
    }

    private fun setupList() {
        rvOrder.apply {
            setUpAsList()
            setAdapter(adapter)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }
        adapter?.setOnActionListener(this)

        rvStatusOrder.apply {
            setUpAsList()
            setAdapter(adapterOrderList)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }
    }

    private fun setupBottomSheet() {
        mBottomSheetBehaviorOrder = BottomSheetBehavior.from(homeContainerOrder)
        mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
        mBottomSheetBehaviorStatus = BottomSheetBehavior.from(homeContainerStatus)
        mBottomSheetBehaviorStatus?.state = BottomSheetBehavior.STATE_HIDDEN
        mBottomSheetBehaviorReview = BottomSheetBehavior.from(homeContainerReview)
        mBottomSheetBehaviorReview?.state = BottomSheetBehavior.STATE_HIDDEN
    }


    private fun setupMap() {
        mapHome.getMapAsync { mapBoxMap ->
            this.mapBox = mapBoxMap
            CommonUtils.setMarker(-10.1612841, 123.6061745, "Air tangki Nostalgia", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1603677, 123.6081796, "Air tangki Gloria", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1545117, 123.6176783, "Air tangki Om Nadus", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6170343, "Air tangki Pieter", R.drawable.ic_car, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6061854, "Your Position", R.drawable.ic_location_blue, this, mapBox)


            mapBox?.setOnMarkerClickListener { marker ->
                if (!marker.title.equals("Your Position", true)) {
                    if (isOrder) {
                        homeOrderTitle.text = marker.title
                        CommonUtils.setCamera(marker.position.latitude, marker.position.longitude, mapBox)
                        mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_EXPANDED
                        val list = mutableListOf<OrderTangki>()
                        var order = OrderTangki()
                        order?.isChoose = true
                        list.add(order)
                        order?.isChoose = false
                        list.add(order)
                        list.add(order)
                        adapter?.clear()
                        adapter?.add(list)
                        rvOrder?.let {
                            it.completeRefresh()
                            it.loadMoreComplete()
                            it.stopShimmer()
                        }

                        rvOrder.showRecycler()
                    }
                }
                true
            }
        }


        homeOrderFormClose.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
        }

    }


    override fun onClicked(view: OrderTangkiItemView) {
        adapter?.notifyDataSetChanged()
    }


    override fun onStart() {
        super.onStart()
        mapHome.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapHome.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapHome.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapHome.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapHome.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapHome.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.menu_history -> startActivity(HistoryActivity.createIntent(this))
            R.id.menu_setting -> startActivity(SettingActivity.createIntent(this))
        }

        return super.onOptionsItemSelected(item)

    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }
    }
}