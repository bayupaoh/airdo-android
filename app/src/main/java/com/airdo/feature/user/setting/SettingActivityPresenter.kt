package com.airdo.feature.user.setting;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class SettingActivityPresenter : BasePresenter<SettingActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: SettingActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: SettingActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}