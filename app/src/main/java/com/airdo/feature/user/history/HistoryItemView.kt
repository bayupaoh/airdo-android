package com.airdo.feature.user.history;

import android.view.View
import com.airdo.base.ui.adapter.BaseRecyclerAdapter
import com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder
import kotlinx.android.synthetic.main.item_history.view.*

/**
 * Created by SuitTemplate
 */

class HistoryItemView(itemView: View)
    : BaseItemViewHolder<Any>(itemView) {
    private var mOnActionListener: HistoryItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: HistoryItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }


    override fun bind(data: Any?) {
        itemView.itemHistoryCard?.setOnClickListener {
            mOnActionListener?.onClicked(this)
        }
    }

    interface OnActionListener {
        fun onClicked(view: HistoryItemView)
    }
}