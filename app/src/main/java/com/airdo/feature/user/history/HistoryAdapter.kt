package com.airdo.feature.user.history;

import android.content.Context
import android.view.ViewGroup
import com.airdo.R;
import com.airdo.base.ui.adapter.BaseRecyclerAdapter;

/**
 * Created by SuitTemplate
 */

public class HistoryAdapter() : BaseRecyclerAdapter<Any, HistoryItemView>() {

    private var mOnActionListener: HistoryItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: HistoryItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_history


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryItemView {
        val view = HistoryItemView(getView(parent!!))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}