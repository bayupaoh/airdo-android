package com.airdo.feature.user.home;

import android.content.Context
import android.view.ViewGroup
import com.airdo.R;
import com.airdo.base.ui.adapter.BaseRecyclerAdapter;
import com.airdo.data.api.model.OrderTangki

/**
 * Created by SuitTemplate
 */

public class OrderTangkiAdapter : BaseRecyclerAdapter<OrderTangki, OrderTangkiItemView>() {
    override fun getItemResourceLayout(): Int {
        return R.layout.item_order_tangki
    }

    private var mOnActionListener: OrderTangkiItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: OrderTangkiItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderTangkiItemView {
        val view = OrderTangkiItemView(getView(parent))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}