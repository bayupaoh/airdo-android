package com.airdo.feature.user.history;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class HistoryActivityPresenter : BasePresenter<HistoryActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: HistoryActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: HistoryActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}