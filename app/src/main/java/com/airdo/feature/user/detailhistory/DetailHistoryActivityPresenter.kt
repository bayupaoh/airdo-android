package com.airdo.feature.user.detailhistory;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class DetailHistoryActivityPresenter : BasePresenter<DetailHistoryActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: DetailHistoryActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: DetailHistoryActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}