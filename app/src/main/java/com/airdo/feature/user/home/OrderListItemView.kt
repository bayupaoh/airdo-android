package com.airdo.feature.user.home;

import android.view.View
import com.airdo.base.ui.adapter.BaseRecyclerAdapter
import com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder
import com.airdo.data.api.model.OrderTangki

/**
 * Created by SuitTemplate
 */

class OrderListItemView(itemView: View)
    : BaseItemViewHolder<OrderTangki>(itemView) {
    private var mOnActionListener: OrderListItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: OrderListItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }


    override fun bind(data: OrderTangki?) {

    }

    interface OnActionListener {
        fun onClicked(view: OrderListItemView)
    }
}