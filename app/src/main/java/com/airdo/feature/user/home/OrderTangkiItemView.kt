package com.airdo.feature.user.home;

import android.util.Log
import android.view.View
import com.airdo.R
import com.airdo.base.ui.adapter.BaseRecyclerAdapter
import com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder
import com.airdo.data.api.model.OrderTangki
import kotlinx.android.synthetic.main.item_order_tangki.view.*

/**
 * Created by SuitTemplate
 */

class OrderTangkiItemView(itemView: View)
    : BaseItemViewHolder<OrderTangki>(itemView) {

    private var mOnActionListener: OrderTangkiItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: OrderTangkiItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun bind(data: OrderTangki?) {
        if (data?.isChoose!!) {
            itemView.itemOrderTangkiDriverName.setTextColor(itemView.resources.getColor(R.color.blue_light))
            itemView.itemOrderTangkiDriverKet.setTextColor(itemView.resources.getColor(R.color.blue_light))
            itemView.itemOrderTangkiDriverPrice.setTextColor(itemView.resources.getColor(R.color.blue_light))
            itemView.itemOrderTangkiDriverOrder.setTextColor(itemView.resources.getColor(R.color.white))
            itemView.itemOrderTangkiDriverOrder.setBackgroundColor(itemView.resources.getColor(R.color.blue_light))
            itemView.itemOrderTangkiDriverOrder.text = "Batal"
        } else {
            itemView.itemOrderTangkiDriverName.setTextColor(itemView.resources.getColor(R.color.gray))
            itemView.itemOrderTangkiDriverKet.setTextColor(itemView.resources.getColor(R.color.gray))
            itemView.itemOrderTangkiDriverPrice.setTextColor(itemView.resources.getColor(R.color.gray))
            itemView.itemOrderTangkiDriverOrder.setTextColor(itemView.resources.getColor(R.color.gray))
            itemView.itemOrderTangkiDriverOrder.setBackgroundColor(itemView.resources.getColor(R.color.backgroundColor))
            itemView.itemOrderTangkiDriverOrder.text = "Pilih"
        }

        itemView.itemOrderTangkiDriverOrder.setOnClickListener {
            data?.isChoose = !data?.isChoose
            bind(data)
        }
    }

    interface OnActionListener {
        fun onClicked(view: OrderTangkiItemView)
        fun onClicked(position: Int, data: OrderTangki?)
    }
}