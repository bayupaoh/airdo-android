package com.airdo.feature.user.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.feature.login.LoginActivity
import kotlinx.android.synthetic.main.activity_setting.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class SettingActivity : BaseActivity(), SettingActivityView {

    private var presenter: SettingActivityPresenter? = null
    private val actionClicked = ::dialogPositiveAction

    override val resourceLayout: Int
        get() = R.layout.activity_setting;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(settingToolbar, true)
        setupPresenter()
        setupOnClick()
    }

    private fun setupOnClick() {
        settingLogout.setOnClickListener {
            showConfirmationDialog("Apakah anda yakin ingin keluar?", actionClicked)
        }
    }

    private fun setupPresenter() {
        presenter = SettingActivityPresenter()
        presenter?.attachView(this)
    }

    private fun dialogPositiveAction() {
        var intent = LoginActivity.createIntent(this)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, SettingActivity::class.java)
        }
    }
}