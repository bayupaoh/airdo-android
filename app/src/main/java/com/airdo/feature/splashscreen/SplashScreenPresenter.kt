package com.airdo.feature.splashscreen

import android.os.Handler
import com.airdo.BaseApplication
import com.airdo.base.presenter.BasePresenter

/**
 * Created by dodydmw19 on 12/19/18.
 */

class SplashScreenPresenter : BasePresenter<SplashScreenView> {

    private var mvpView: SplashScreenView? = null
    val SPLASH_TIME: Long = 3000

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        detachView()
    }

    fun initialize() {
        Handler().postDelayed({ mvpView?.navigateToMainView() }, SPLASH_TIME)
    }

    override fun attachView(view: SplashScreenView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}