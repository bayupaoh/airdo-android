package com.airdo.feature.splashscreen

import com.airdo.base.presenter.MvpView

/**
 * Created by dodydmw19 on 12/19/18.
 */

interface SplashScreenView : MvpView {

    fun navigateToMainView()

}