package com.airdo.feature.splashscreen

import android.content.Intent
import android.os.Bundle
import com.airdo.R
import com.airdo.base.ui.BaseActivity
import com.airdo.feature.login.LoginActivity
import com.airdo.helper.CommonConstant

/**
 * Created by dodydmw19 on 12/19/18.
 */

class SplashScreenActivity : BaseActivity(), SplashScreenView {

    private var splashScreenPresenter: SplashScreenPresenter? = null
    private val actionClicked = ::dialogPositiveAction

    override val resourceLayout: Int = R.layout.activity_splashscreen

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
    }

    override fun onResume() {
        super.onResume()
        setupPresenter()
    }
    private fun handleIntent(){
        val data: Bundle? = intent.extras
        if(data?.getString(CommonConstant.APP_CRASH) != null){
            showConfirmationSingleDialog(getString(R.string.txt_error_crash), actionClicked)
        }else{
            splashScreenPresenter?.initialize()
        }
    }

    private fun setupPresenter() {
        splashScreenPresenter = SplashScreenPresenter()
        splashScreenPresenter?.attachView(this)
        handleIntent()
    }

    override fun navigateToMainView() {
        var intent = LoginActivity.createIntent(this)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun dialogPositiveAction() {
        splashScreenPresenter?.initialize()
    }

}