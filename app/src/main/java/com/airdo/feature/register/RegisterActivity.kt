package com.airdo.feature.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import kotlinx.android.synthetic.main.activity_register.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class RegisterActivity : BaseActivity(), RegisterActivityView {

    private var presenter: RegisterActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_register;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar()
        setupPresenter()
        actionClicked()
    }

    private fun setupToolbar() {
        setSupportActionBar(registerToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    private fun actionClicked() {
        registerSubmit.setOnClickListener {
            finish()
        }
    }

    private fun setupPresenter() {
        presenter = RegisterActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }
}