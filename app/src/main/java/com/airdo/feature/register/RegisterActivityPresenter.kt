package com.airdo.feature.register;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class RegisterActivityPresenter : BasePresenter<RegisterActivityView> {

    private var view: RegisterActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: RegisterActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }

    override fun onPause() {

    }

    override fun onDestroy() {

    }

}