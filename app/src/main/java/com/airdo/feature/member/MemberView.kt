package com.airdo.feature.member

import com.airdo.base.presenter.MvpView
import com.airdo.data.api.model.User

/**
 * Created by DODYDMW19 on 1/30/2018.
 */

interface MemberView : MvpView {

    fun onMemberCacheLoaded(members: List<User>?)

    fun onMemberLoaded(members: List<User>?)

    fun onMemberEmpty()

    fun onFailed(error: Any?)

}