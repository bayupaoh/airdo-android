package com.airdo.feature.member

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.airdo.R
import com.airdo.base.ui.BaseActivity
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import com.airdo.data.api.model.ErrorCodeHelper
import com.airdo.data.api.model.User
import kotlinx.android.synthetic.main.activity_member.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

/**
 * Created by DODYDMW19 on 1/30/2018.
 */

class MemberActivity : BaseActivity(),
        MemberView,
        MemberItemView.OnActionListener {

    private var memberPresenter: MemberPresenter? = null
    private var currentPage: Int = 1
    private var memberAdapter: MemberAdapter? = MemberAdapter()

    override val resourceLayout: Int = R.layout.activity_member

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupProgressView()
        setupEmptyView()
        setupErrorView()
        setupList()
        Handler().postDelayed({ setupPresenter() }, 100)
    }

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, MemberActivity::class.java)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        clearRecyclerView(rvMember)
    }

    private fun setupPresenter() {
        memberPresenter = MemberPresenter()
        memberPresenter?.attachView(this)
        memberPresenter?.getMemberCache()

        rvMember.hideEmpty()
        rvMember.showShimmer()
        memberPresenter?.getMember(currentPage)
    }

    private fun setupList() {
        rvMember.apply {
            setUpAsList()
            setAdapter(memberAdapter)
            setPullToRefreshEnable(true)
            setLoadingMoreEnabled(true)
            setLoadingListener(object : XRecyclerView.LoadingListener {
                override fun onRefresh() {
                    loadData(1)
                }

                override fun onLoadMore() {
                    currentPage++
                    loadData(currentPage)
                }
            })
        }
        memberAdapter?.setOnActionListener(this)
        rvMember.showShimmer()
    }

    private fun loadData(page: Int){
        memberPresenter?.getMember(page)
    }

    private fun setData(data: List<User>?) {
        data?.let {
            if (currentPage == 1) {
                memberAdapter?.clear()
            }
            memberAdapter?.add(it)
        }
        rvMember.stopShimmer()
        rvMember.showRecycler()
    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        rvMember.setImageEmptyView(R.drawable.empty_state)
        rvMember.setTextEmptyView(getString(R.string.txt_empty_member))
        rvMember.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData(1)
            }

        })
    }

    private fun setupErrorView() {
        rvMember.setImageErrorView(R.drawable.empty_state)
        rvMember.setTExtErrorView(getString(R.string.txt_error_connection))
        rvMember.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData(1)
            }

        })
    }

    override fun onMemberCacheLoaded(members: List<User>?) {
        members.let {
            if (members?.isNotEmpty()!!) {
                setData(members)
            }
        }
        finishLoad(rvMember)
    }

    override fun onMemberLoaded(members: List<User>?) {
        members.let {
            if (members?.isNotEmpty()!!) {
                setData(members)
            }
        }
        finishLoad(rvMember)
    }

    override fun onMemberEmpty() {
        finishLoad(rvMember)
        rvMember.showEmpty()
    }

    override fun onFailed(error: Any?) {
        finishLoad(rvMember)
        rvMember.showEmpty()
        error?.let { ErrorCodeHelper.getErrorMessage(this, it)?.let { msg -> showToast(msg) } }
    }

    override fun onClicked(view: MemberItemView?) {
        view?.getData()?.let {
            showToast(it.firstName.toString())
        }
    }
}