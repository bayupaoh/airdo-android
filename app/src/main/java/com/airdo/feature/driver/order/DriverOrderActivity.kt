package com.airdo.feature.driver.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior
import android.view.View
import android.widget.RelativeLayout
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.helper.CommonUtils
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.activity_driver_order.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class DriverOrderActivity : BaseActivity(), DriverOrderActivityView {

    private var presenter: DriverOrderActivityPresenter? = null
    private var mapBox: MapboxMap? = null
    private var mBottomSheetBehaviorOrder: BottomSheetBehavior<RelativeLayout>? = null
    private var mBottomSheetBehaviorDenied: BottomSheetBehavior<RelativeLayout>? = null

    override val resourceLayout: Int
        get() = R.layout.activity_driver_order;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(driverOrderToolbar, true)
        setupPresenter()
        setupMap()
        setupBottomSheet()
        setupOnCLick()
    }

    private fun setupOnCLick() {
        driverOrderClose.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        driverOrderDeniedClose.setOnClickListener {
            mBottomSheetBehaviorDenied?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        driverOrderSubmit.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_EXPANDED
        }

        driverOrderDismiss.setOnClickListener {
            mBottomSheetBehaviorDenied?.state = BottomSheetBehavior.STATE_EXPANDED
        }

        driverOrderSubmitConfirm.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
            finish()
        }

        driverOrderDeniedSubmit.setOnClickListener {
            mBottomSheetBehaviorDenied?.state = BottomSheetBehavior.STATE_HIDDEN
            finish()
        }
    }

    private fun setupBottomSheet() {
        mBottomSheetBehaviorOrder = BottomSheetBehavior.from(driverOrderStatus)
        mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
        mBottomSheetBehaviorDenied = BottomSheetBehavior.from(driverOrderDenied)
        mBottomSheetBehaviorDenied?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun setupMap() {
        driverOrderMap.getMapAsync { mapBoxMap ->
            this.mapBox = mapBoxMap

            CommonUtils.setMarker(-10.1556097, 123.6061854, "Asal", R.drawable.ic_location_blue, this, mapBox)
            CommonUtils.setMarker(-10.1556097, 123.6170343, "Tujuan", R.drawable.ic_location_blue, this, mapBox)
        }
    }

    private fun setupPresenter() {
        presenter = DriverOrderActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        driverOrderMap.onDestroy()

    }


    override fun onStart() {
        super.onStart()
        driverOrderMap.onStart()
    }

    override fun onResume() {
        super.onResume()
        driverOrderMap.onResume()
    }

    override fun onPause() {
        super.onPause()
        driverOrderMap.onPause()
    }

    override fun onStop() {
        super.onStop()
        driverOrderMap.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        driverOrderMap.onLowMemory()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, DriverOrderActivity::class.java)
        }
    }
}