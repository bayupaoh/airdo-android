package com.airdo.feature.driver.review;

import android.content.Context
import android.view.ViewGroup
import com.airdo.R;
import com.airdo.base.ui.adapter.BaseRecyclerAdapter;

/**
 * Created by SuitTemplate
 */

public class ReviewActivityAdapter() : BaseRecyclerAdapter<Any, ReviewActivityItemView>() {
    private var mOnActionListener: ReviewActivityItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: ReviewActivityItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_driver_review


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewActivityItemView {
        val view = ReviewActivityItemView(getView(parent!!))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}