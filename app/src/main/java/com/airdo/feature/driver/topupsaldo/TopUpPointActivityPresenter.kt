package com.airdo.feature.driver.topupsaldo;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class TopUpPointActivityPresenter : BasePresenter<TopUpPointActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: TopUpPointActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: TopUpPointActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}