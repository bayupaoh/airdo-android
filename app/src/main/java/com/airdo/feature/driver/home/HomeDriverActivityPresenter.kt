package com.airdo.feature.driver.home;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class HomeDriverActivityPresenter : BasePresenter<HomeDriverActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: HomeDriverActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: HomeDriverActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}