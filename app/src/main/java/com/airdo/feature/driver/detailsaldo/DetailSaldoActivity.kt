package com.airdo.feature.driver.detailsaldo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.feature.driver.konfirmasi.KonfirmasiActivity
import kotlinx.android.synthetic.main.activity_detail_saldo.*

class DetailSaldoActivity : BaseActivity(), DetailSaldoActivityView {

    private var presenter: DetailSaldoActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_detail_saldo;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(detailSaldoToolbar, true)
        setupPresenter()
        setupOnClick()
    }

    private fun setupOnClick() {
        detailSaldoConfirm.setOnClickListener {
            startActivity(KonfirmasiActivity.createIntent(this))
        }
    }

    private fun setupPresenter() {
        presenter = DetailSaldoActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, DetailSaldoActivity::class.java)
        }
    }
}