package com.airdo.feature.driver.review;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class ReviewActivity : BaseActivity(), ReviewActivityView {

    private var presenter: ReviewActivityPresenter? = null
    private var adapterReview : ReviewActivityAdapter ?= ReviewActivityAdapter()

    override val resourceLayout: Int
        get() = R.layout.activity_review;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(reviewDriverToolbar,true)
        setupPresenter()
        setupEmptyView()
        setupErrorView()
        setupList()
    }

    private fun setupPresenter() {
        presenter = ReviewActivityPresenter()
        presenter?.attachView(this)
    }

    private fun setupList() {
        var list = mutableListOf<Any>()
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")

        reviewDriverRec.apply {
            setUpAsList()
            setAdapter(adapterReview)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }


        adapterReview?.add(list)

        reviewDriverRec?.let {
            it.completeRefresh()
            it.loadMoreComplete()
            it.stopShimmer()
        }

        reviewDriverRec.showRecycler()

    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        reviewDriverRec.setImageEmptyView(R.drawable.empty_state)
        reviewDriverRec.setTextEmptyView(getString(R.string.txt_empty_member))
        reviewDriverRec.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupErrorView() {
        reviewDriverRec.setImageErrorView(R.drawable.empty_state)
        reviewDriverRec.setTExtErrorView(getString(R.string.txt_error_connection))
        reviewDriverRec.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })

    }
    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, ReviewActivity::class.java)
        }
    }
}