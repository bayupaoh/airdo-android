package com.airdo.feature.driver.order;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class DriverOrderActivityPresenter : BasePresenter<DriverOrderActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: DriverOrderActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: DriverOrderActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}