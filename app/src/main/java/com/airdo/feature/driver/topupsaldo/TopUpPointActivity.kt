package com.airdo.feature.driver.topupsaldo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.data.prefs.SuitPreferences
import com.airdo.feature.driver.detailsaldo.DetailSaldoActivity
import com.airdo.feature.driver.konfirmasi.KonfirmasiActivity
import com.airdo.helper.CommonConstant
import com.mapbox.mapboxsdk.style.layers.Property
import kotlinx.android.synthetic.main.activity_top_up_point.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class TopUpPointActivity : BaseActivity(), TopUpPointActivityView {

    private var mBottomSheetBehaviorTopUp: BottomSheetBehavior<LinearLayout>? = null
    private var presenter: TopUpPointActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_top_up_point;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(topUpToolbar,true)
        setupPresenter()
        setupBottomSheet()
        setupButton()
        setupOnCLick()
    }

    private fun setupButton() {
        SuitPreferences.instance()?.getBoolean(CommonConstant.PREFERENCES_IS_TRANSFER)?.let {
            if (it) {
                topUpConfirmButton.visibility = VISIBLE
                topUpOnlineButton.visibility = GONE
            } else {
                topUpConfirmButton.visibility = GONE
                topUpOnlineButton.visibility = VISIBLE
            }
        }
    }

    private fun setupBottomSheet() {
        mBottomSheetBehaviorTopUp = BottomSheetBehavior.from(topUpDialog)
        mBottomSheetBehaviorTopUp?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun setupOnCLick() {
        topUpOnlineButton.setOnClickListener {
            mBottomSheetBehaviorTopUp?.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        }

        driverOrderDeniedClose.setOnClickListener {
            mBottomSheetBehaviorTopUp?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        topUpDialogSubmit.setOnClickListener {
            mBottomSheetBehaviorTopUp?.state = BottomSheetBehavior.STATE_HIDDEN
            topUpNominal.setText("")
            SuitPreferences.instance()?.saveBoolean(CommonConstant.PREFERENCES_IS_TRANSFER,true)
            startActivity(DetailSaldoActivity.createIntent(this))
        }

        topUpConfirmButton.setOnClickListener {
            startActivity(KonfirmasiActivity.createIntent(this))
        }
    }

    private fun setupPresenter() {
        presenter = TopUpPointActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        setupButton()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, TopUpPointActivity::class.java)
        }
    }
}