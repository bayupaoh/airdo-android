package com.airdo.feature.driver.konfirmasi;

import android.Manifest
import android.content.Context;
import android.content.Intent;
import android.net.Uri
import android.os.Bundle;
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.Window
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.data.prefs.SuitPreferences
import com.airdo.helper.CommonConstant
import com.esafirm.imagepicker.features.ImagePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_konfirmasi.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.File

class KonfirmasiActivity : BaseActivity(), KonfirmasiActivityView {

    private var presenter: KonfirmasiActivityPresenter? = null
    private var confirmDialog: AlertDialog ?= null
//    private val actionClicked = ::dialogPositiveAction

    override val resourceLayout: Int
        get() = R.layout.activity_konfirmasi;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(konfirmasiToolbar, true)
        setupPresenter()
        setupOnClick()
    }

    private fun setupOnClick() {
        konfirmasiPhotoButton.setOnClickListener {
            showConfirmationDialog("Pilih sumber gambar?")
        }

        konfimasiUpload.setOnClickListener {
            SuitPreferences.instance()?.saveBoolean(CommonConstant.PREFERENCES_IS_TRANSFER,false)
            finish()
        }
    }

    fun showConfirmationDialog(message: String) {
        confirmDialog = AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Kamera") { _, _ ->
                    actionOpenCamera()
                }
                .setNegativeButton("Galeri") { _, _ ->
                    actionOpenGalery()
                }
                .create()

        confirmDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        confirmDialog?.show()
    }

    private fun actionOpenCamera() {
        confirmDialog?.dismiss()
        setPermission(true)
    }

    private fun actionOpenGalery() {
        confirmDialog?.dismiss()
        setPermission(false)
    }

    private fun setupPresenter() {
        presenter = KonfirmasiActivityPresenter()
        presenter?.attachView(this)
    }


    private fun setPermission(isFromCamera: Boolean) {
        val feedbackViewMultiplePermissionListener = object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                if (report?.areAllPermissionsGranted()!!) {
                    if (isFromCamera)
                        ImagePicker.cameraOnly().start(this@KonfirmasiActivity)
                    else {
                        ImagePicker.create(this@KonfirmasiActivity).showCamera(false).single().start()
                    }
                } else {
                    setPermission(isFromCamera)
                }
            }

            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {

            }

        }

        val errorListener = object : PermissionRequestErrorListener {
            override fun onError(error: DexterError?) {

            }

        }

        var allPermissionsListener = CompositeMultiplePermissionsListener(feedbackViewMultiplePermissionListener,
                SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(konfirmasiCoordinator,
                        "CAMERA and WRITE FILE PERMISSION are needed to send image")
                        .withButton("Try Again", object : View.OnClickListener {
                            override fun onClick(v: View?) {
                                setPermission(isFromCamera)
                            }

                        }).build())

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(allPermissionsListener)
                .withErrorListener(errorListener)
                .check();
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image = ImagePicker.getFirstImageOrNull(data)
            konfirmasiPhoto.setImageURI(Uri.fromFile(File(image?.path)))
            val compressedImage = Compressor(this)?.compressToFile(File(image?.path))
            compressedImage?.path?.let {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, KonfirmasiActivity::class.java)
        }
    }
}