package com.airdo.feature.driver.history;

import android.content.Context
import android.view.ViewGroup
import com.airdo.R;
import com.airdo.base.ui.adapter.BaseRecyclerAdapter;

/**
 * Created by SuitTemplate
 */

public class HistoryDriverActivityAdapter() : BaseRecyclerAdapter<Any, HistoryDriverActivityItemView>() {
    private var mOnActionListener: HistoryDriverActivityItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: HistoryDriverActivityItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }

    override fun getItemResourceLayout(): Int = R.layout.item_history_driver


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryDriverActivityItemView {
        val view = HistoryDriverActivityItemView(getView(parent!!))
        mOnActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}