package com.airdo.feature.driver.detailsaldo;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class DetailSaldoActivityPresenter : BasePresenter<DetailSaldoActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: DetailSaldoActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: DetailSaldoActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}