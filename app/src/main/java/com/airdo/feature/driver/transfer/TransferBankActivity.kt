package com.airdo.feature.driver.transfer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class TransferBankActivity : BaseActivity(), TransferBankActivityView {

    private var presenter: TransferBankActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_transfer_bank;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
    }

    private fun setupPresenter() {
        presenter = TransferBankActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, TransferBankActivity::class.java)
        }
    }
}