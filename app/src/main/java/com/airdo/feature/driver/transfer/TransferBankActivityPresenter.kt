package com.airdo.feature.driver.transfer;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class TransferBankActivityPresenter : BasePresenter<TransferBankActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: TransferBankActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: TransferBankActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}