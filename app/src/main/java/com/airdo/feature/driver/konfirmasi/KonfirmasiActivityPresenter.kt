package com.airdo.feature.driver.konfirmasi;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class KonfirmasiActivityPresenter : BasePresenter<KonfirmasiActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: KonfirmasiActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: KonfirmasiActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}