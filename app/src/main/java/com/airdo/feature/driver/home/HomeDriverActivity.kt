package com.airdo.feature.driver.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior
import android.view.View
import android.widget.RelativeLayout
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import com.airdo.feature.driver.history.HistoryDriverActivity
import com.airdo.feature.driver.history.HistoryDriverActivityAdapter
import com.airdo.feature.driver.order.DriverOrderActivity
import com.airdo.feature.driver.review.ReviewActivity
import com.airdo.feature.driver.review.ReviewActivityAdapter
import com.airdo.feature.driver.topupsaldo.TopUpPointActivity
import com.airdo.feature.user.history.HistoryItemView
import kotlinx.android.synthetic.main.activity_home_driver.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

class HomeDriverActivity : BaseActivity(), HomeDriverActivityView, HistoryItemView.OnActionListener {

    private var presenter: HomeDriverActivityPresenter? = null
    private var mBottomSheetBehaviorOrder: BottomSheetBehavior<RelativeLayout>? = null
    private var adapterHistory : HistoryDriverActivityAdapter ?= HistoryDriverActivityAdapter()
    private var adapterReview : ReviewActivityAdapter ?= ReviewActivityAdapter()

    override val resourceLayout: Int
        get() = R.layout.activity_home_driver;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setupToolbar()
        setupBottomSheet()
        setupOnCLick()
        setupEmptyView()
        setupErrorView()
        setupList()
    }

    private fun setupToolbar() {
        setupToolbar(mainDriverToolbar,false)
    }

    private fun setupOnCLick() {
        mainDriverShowDialog.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_EXPANDED
        }

        mainDriverDialogAccept.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
            startActivity(DriverOrderActivity.createIntent(this))
        }

        mainDriverDialogDenied.setOnClickListener {
            mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        mainDriverMoreHistory.setOnClickListener {
            startActivity(HistoryDriverActivity.createIntent(this))
        }

        mainDriverMoreReview.setOnClickListener {
            startActivity(ReviewActivity.createIntent(this))
        }

        mainDriverTopUp.setOnClickListener {
            startActivity(TopUpPointActivity.createIntent(this))
        }
    }

    private fun setupBottomSheet() {
        mBottomSheetBehaviorOrder = BottomSheetBehavior.from(mainDriverRelDialogOrder)
        mBottomSheetBehaviorOrder?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun setupList() {
        var list = mutableListOf<Any>()
        list.add("")
        list.add("")
        list.add("")

        mainDriverRecHistory.apply {
            setUpAsList()
            setAdapter(adapterHistory)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }

        mainDriverRecRating.apply {
            setUpAsList()
            setAdapter(adapterReview)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }

        adapterHistory?.add(list)
        adapterReview?.add(list)
//        adapterHistory?.setOnActionListener(this)

        mainDriverRecHistory?.let {
            it.completeRefresh()
            it.loadMoreComplete()
            it.stopShimmer()
        }

        mainDriverRecRating?.let {
            it.completeRefresh()
            it.loadMoreComplete()
            it.stopShimmer()
        }

        mainDriverRecHistory.showRecycler()
        mainDriverRecRating.showRecycler()

    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        mainDriverRecHistory.setImageEmptyView(R.drawable.empty_state)
        mainDriverRecHistory.setTextEmptyView(getString(R.string.txt_empty_member))
        mainDriverRecHistory.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })

        mainDriverRecRating.setImageEmptyView(R.drawable.empty_state)
        mainDriverRecRating.setTextEmptyView(getString(R.string.txt_empty_member))
        mainDriverRecRating.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupErrorView() {
        mainDriverRecHistory.setImageErrorView(R.drawable.empty_state)
        mainDriverRecHistory.setTExtErrorView(getString(R.string.txt_error_connection))
        mainDriverRecHistory.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })

        mainDriverRecRating.setImageErrorView(R.drawable.empty_state)
        mainDriverRecRating.setTExtErrorView(getString(R.string.txt_error_connection))
        mainDriverRecRating.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupPresenter() {
        presenter = HomeDriverActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onClicked(view: HistoryItemView) {

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, HomeDriverActivity::class.java)
        }
    }
}