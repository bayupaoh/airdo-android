package com.airdo.feature.driver.history;

import android.view.View
import com.airdo.base.ui.adapter.BaseRecyclerAdapter
import com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder

/**
 * Created by SuitTemplate
 */

class HistoryDriverActivityItemView(itemView: View)
    : BaseItemViewHolder<Any>(itemView) {
    private var mOnActionListener: HistoryDriverActivityItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: HistoryDriverActivityItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }


    override fun bind(data: Any?) {

    }

    interface OnActionListener {
        fun onClicked(view: HistoryDriverActivityItemView)
    }
}