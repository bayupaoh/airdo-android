package com.airdo.feature.driver.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.base.ui.recyclerview.BaseRecyclerView
import kotlinx.android.synthetic.main.activity_history_driver.*
import kotlinx.android.synthetic.main.layout_base_shimmer.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class HistoryDriverActivity : BaseActivity(), HistoryDriverActivityView {

    private var presenter: HistoryDriverActivityPresenter? = null
    private var adapterHistory : HistoryDriverActivityAdapter ?= HistoryDriverActivityAdapter()

    override val resourceLayout: Int
        get() = R.layout.activity_history_driver;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(historyDriverToolbar,true)
        setupPresenter()
        setupEmptyView()
        setupErrorView()
        setupList()
    }

    private fun setupPresenter() {
        presenter = HistoryDriverActivityPresenter()
        presenter?.attachView(this)
    }

    private fun setupList() {
        var list = mutableListOf<Any>()
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")

        recHistoryDriver.apply {
            setUpAsList()
            setAdapter(adapterHistory)
            setPullToRefreshEnable(false)
            setLoadingMoreEnabled(false)
        }


        adapterHistory?.add(list)

        recHistoryDriver?.let {
            it.completeRefresh()
            it.loadMoreComplete()
            it.stopShimmer()
        }

        recHistoryDriver.showRecycler()

    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            viewStub.layoutResource = this
        }

        viewStub.inflate()
    }

    private fun setupEmptyView() {
        recHistoryDriver.setImageEmptyView(R.drawable.empty_state)
        recHistoryDriver.setTextEmptyView(getString(R.string.txt_empty_member))
        recHistoryDriver.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })
    }

    private fun setupErrorView() {
        recHistoryDriver.setImageErrorView(R.drawable.empty_state)
        recHistoryDriver.setTExtErrorView(getString(R.string.txt_error_connection))
        recHistoryDriver.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {

            }

        })

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

     fun getContext(): Context {
        return this
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, HistoryDriverActivity::class.java)
        }
    }
}