package com.airdo.feature.driver.history;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class HistoryDriverActivityPresenter : BasePresenter<HistoryDriverActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: HistoryDriverActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: HistoryDriverActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}