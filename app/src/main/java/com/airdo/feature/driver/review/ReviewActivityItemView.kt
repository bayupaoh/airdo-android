package com.airdo.feature.driver.review;

import android.view.View
import com.airdo.base.ui.adapter.BaseRecyclerAdapter
import com.airdo.base.ui.adapter.viewholder.BaseItemViewHolder

/**
 * Created by SuitTemplate
 */

class ReviewActivityItemView(itemView: View)
    : BaseItemViewHolder<Any>(itemView) {
    private var mOnActionListener: ReviewActivityItemView.OnActionListener? = null

    fun setOnActionListener(onActionListener: ReviewActivityItemView.OnActionListener) {
        mOnActionListener = onActionListener
    }


    override fun bind(data: Any?) {

    }

    interface OnActionListener {
        fun onClicked(view: ReviewActivityItemView)
    }
}