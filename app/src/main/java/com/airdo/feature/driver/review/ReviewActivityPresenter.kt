package com.airdo.feature.driver.review;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class ReviewActivityPresenter : BasePresenter<ReviewActivityView> {
    override fun onPause() {

    }

    override fun onDestroy() {

    }

    private var view: ReviewActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: ReviewActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }
}