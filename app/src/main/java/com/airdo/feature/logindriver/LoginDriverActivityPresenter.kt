package com.airdo.feature.logindriver;

import com.airdo.base.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by SuitTemplate
 */

class LoginDriverActivityPresenter : BasePresenter<LoginDriverActivityView> {

    private var view: LoginDriverActivityView? = null
    private val mCompositeDisposable: CompositeDisposable? = null

    override fun attachView(view: LoginDriverActivityView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        mCompositeDisposable?.clear()
    }

    override fun onPause() {

    }

    override fun onDestroy() {

    }

}