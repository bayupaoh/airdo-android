package com.airdo.feature.logindriver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.airdo.R;
import com.airdo.base.ui.BaseActivity;
import com.airdo.feature.driver.home.HomeDriverActivity
import kotlinx.android.synthetic.main.activity_login_driver.*
import kotlinx.android.synthetic.main.activity_register.*

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class LoginDriverActivity : BaseActivity(), LoginDriverActivityView {

    private var presenter: LoginDriverActivityPresenter? = null

    override val resourceLayout: Int
        get() = R.layout.activity_login_driver;

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar()
        setupPresenter()
        actionClicked()
    }

    private fun setupToolbar() {
        setSupportActionBar(loginDriverToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    private fun actionClicked() {
        loginDriverSubmit.setOnClickListener {
            val intent = HomeDriverActivity.createIntent(this)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    private fun setupPresenter() {
        presenter = LoginDriverActivityPresenter()
        presenter?.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, LoginDriverActivity::class.java)
        }
    }
}