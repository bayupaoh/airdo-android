package com.airdo.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.airdo.R
import com.airdo.base.ui.BaseActivity
import com.airdo.feature.logindriver.LoginDriverActivity
import com.airdo.feature.member.MemberActivity
import com.airdo.feature.register.RegisterActivity
import com.airdo.feature.user.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by dodydmw19 on 7/18/18.
 */

class LoginActivity : BaseActivity(), LoginView{


    override val resourceLayout: Int = R.layout.activity_login

    override fun onViewReady(savedInstanceState: Bundle?) {
        actionClicked()
    }


    private fun actionClicked() {
        loginSubmit.setOnClickListener {
            var intent = HomeActivity.createIntent(this)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        loginDriver.setOnClickListener {
            startActivity(LoginDriverActivity.createIntent(this))
        }

        loginRegister.setOnClickListener {
            startActivity(RegisterActivity.createIntent(this))
        }
    }

    companion object {

        fun createIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}