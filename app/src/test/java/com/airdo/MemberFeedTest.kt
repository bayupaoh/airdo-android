package com.airdo

import android.content.Context
import com.airdo.BaseApplication.Companion.applicationComponent
import com.airdo.data.api.model.User
import com.airdo.di.component.DaggerApplicationComponent
import com.airdo.di.module.ContextModule
import com.airdo.feature.member.MemberPresenter
import com.airdo.feature.member.MemberView
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Created by dodydmw19 on 3/8/19.
 */

class MemberFeedTest  {

    @Mock
    lateinit var view: MemberView

    private var presenter: MemberPresenter? = null
    private var items: List<User>? = emptyList()

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        applicationComponent  = DaggerApplicationComponent.builder().contextModule(ContextModule(mock(Context::class.java))).build()
        presenter = MemberPresenter()
    }

    @Test
    fun loadMemberList(){
        presenter?.attachView(view)
        presenter?.getMember(1)
        verify(presenter)?.getMember(1)
        verify(view).onMemberLoaded(items)
    }


}